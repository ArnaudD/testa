#include <QApplication>
#include "gui/Window.h"

using namespace std;

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	app.setWindowIcon(QIcon(":/icons/main.png"));

	Window m_window;
	m_window.showMaximized();

	return app.exec();
}
