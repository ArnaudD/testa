#include "gui/View.h"
#include <math.h>
#include "core/support/Scene.h"
#include "core/3D/Scene3D.h"

View::View(Scene* scene_, QWidget* parent_) : QGraphicsView(parent_)
{
	m_scene = scene_;
	m_dragOn=false;

	setScene(m_scene);
	setRenderHint(QPainter::Antialiasing);
	setTransformationAnchor(QGraphicsView::NoAnchor);
	setRenderHint(QPainter::TextAntialiasing);
	setBackgroundBrush(QBrush(Qt::black, Qt::SolidPattern));
	scale(1,-1);
	setViewport(new Scene3D(this)); //Support openGL
	viewport()->setMouseTracking(true);
}

void View::mousePressEvent(QMouseEvent* event_) 
{
	if (event_->button() == Qt::MidButton)
	{
		m_dragOn = true;
		setCursor(Qt::OpenHandCursor);
	}
	else
	{
		QGraphicsView::mousePressEvent(event_);
	}
}

void View::mouseMoveEvent(QMouseEvent* event_) 
{
	if (m_dragOn==false)
		m_oldMousePos = event_->pos();
	else
	{
		QPoint movement = event_->pos() - m_oldMousePos;
		scrollView(movement);
		m_oldMousePos = event_->pos();
	}

	QGraphicsView::mouseMoveEvent(event_);
}

void View::mouseReleaseEvent(QMouseEvent* event_) 
{
	if (event_->button() == Qt::MidButton) 
	{
		m_dragOn = false;
		
		if(m_scene->isDrawingActive() == true)
			setCursor(Qt::CrossCursor);
		else
			setCursor(Qt::ArrowCursor);
		
	}
	else
	{
		QGraphicsView::mouseReleaseEvent(event_);
	}
}

void View::wheelEvent(QWheelEvent* event_) 
{
	QPoint vpos1 = event_->pos();
	QPointF spos1 = mapToScene(vpos1);
	qreal s = pow((double)2, event_->delta() / 360.0);
	if (!scaleView(s)) return;
	QPoint vpos2 = mapFromScene(spos1);
	QPoint dp = vpos2 - vpos1;
	scrollView(-dp);
}

bool View::scaleView(qreal scaleFactor_)
{
	qreal factor = matrix().scale(scaleFactor_, scaleFactor_).mapRect(QRectF(0, 0, 1, 1)).width();
	if (factor < 0.1 || factor > 100)
		return false;

	scale(scaleFactor_, scaleFactor_);
	return true;
}

void View::scrollView(QPoint dp_)
{
	QScrollBar* hscroll = horizontalScrollBar();
	QScrollBar* vscroll = verticalScrollBar();
	if (hscroll)
	{
		int v = hscroll->value();
		hscroll->setValue(v - dp_.x());
	}
	if (vscroll)
	{
		int v = vscroll->value();
		vscroll->setValue(v - dp_.y());
	}
}

Scene *View::scene()
{
	return m_scene;
}
