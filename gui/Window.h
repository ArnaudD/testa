#ifndef WINDOW_H_INCLUDED
#define WINDOW_H_INCLUDED

#include <QtWidgets>
#include "gui/View.h"
#include "gui/MdiSubWindow.h"
#include "core/support/Layer.h"
#include "core/support/IO.h"
#include "core/support/SnappingNode.h"

class Scene;

class Window : public QMainWindow
{
	Q_OBJECT

	public:
		Window();
		
		void setCoordStatusBar(qreal, qreal);

		void setSelectionButton(bool);
		void setPointButton(bool);
		void setLineButton(bool);
		void setPolylineButton(bool);
		void setCircleButton(bool);
		void setRectButton(bool);
		void setArcButton(bool);
		void setTextButton(bool);
		void setImageButton(bool);
		void setFontActions(bool);
		void setTranslateButton(bool);
		void setRotateButton(bool);
		void setScaleButton(bool);
		void setCopyButton(bool);
		void setCreateBlock(bool);
		
		QString openImageDialog();

		void closedWindow();
		
	protected:
		virtual void keyPressEvent(QKeyEvent*);
		virtual void closeEvent(QCloseEvent*);

	private slots:
		void slot_newScene();
		void slot_fileOpen();
		void slot_fileSave();
		void slot_fileSaveAs();
		void slot_fileCloseAll();
		
		void slot_drawPoint();
		void slot_drawLine();
		void slot_drawPolyline();
		void slot_drawCircle();
		void slot_drawRect();
		void slot_drawArc();
		void slot_drawText();
		void slot_drawImage();
		
		void slot_fontType();
		void slot_fontColor();

		void slot_changeLayer(QString);

		void slot_newLayer();
		void slot_deleteLayer();
		void slot_nameLayer();
		void slot_colorLayer();
		void slot_hideLayer();
		void slot_upLayerLevel();
		void slot_downLayerLevel();
		
		void slot_ortho();
		void slot_snapping();
		void slot_snappingVertices();
		void slot_snappingNearest();
		void slot_snappingMiddle();
		void slot_snappingCenter();
		
		void slot_selection();
		void slot_translate();
		void slot_rotate();
		void slot_scale();
		void slot_copy();
		void slot_createBlock();
		void slot_explodeBlock();
		
		void slot_subWindowActivated();
		
	private:
		void setEditCursor(bool);
		void fillComboLayers();
		void updateCombo();
		
		QMdiArea* m_centralArea;

		//Declaration menu Files
		QMenu* m_menuFile;
		QToolBar* m_toolbarFile;
		
		QAction* m_actionNew;
		QAction* m_actionOpen;
		QAction* m_actionSave;
		QAction* m_actionSaveAs;
		QAction* m_actionCloseAll;
		QAction* m_actionExit;
		
		// Declaration menu Layers
		QMenu* m_menuLayers;
		QToolBar* m_toolbarLayers;
		
		QAction* m_actionNewLayer;
		QAction* m_actionDeleteLayer;
		QAction* m_actionRenameLayer;
		QAction* m_actionColorLayer;
		QAction* m_actionToggleLayer;
		QAction* m_actionRaiseLayer;
		QAction* m_actionLowerLayer;

		// Declaration menu Drawing
		QMenu* m_menuDrawing;
		QToolBar* m_toolbarDrawing;
		
		QAction* m_actionSelection;
		QAction* m_actionDrawPoint;
		QAction* m_actionDrawLine;
		QAction* m_actionDrawPolyline;
		QAction* m_actionDrawCircle;
		QAction* m_actionDrawRect;
		QAction* m_actionDrawArc;
		QAction* m_actionDrawText;
		QAction* m_actionDrawImage;
		
		// Declaration menu Modify
		QMenu* m_menuModify;
		QToolBar* m_toolbarModify;
		
		QAction* m_actionTranslate;
		QAction* m_actionRotate;
		QAction* m_actionScale;
		QAction* m_actionCopy;
		QAction* m_actionCreateBlock;
		QAction* m_actionExplodeBlock;
		QAction* m_actionFontType;
		QAction* m_actionFontColor;
	
		// Declaration menu Constraints
		QMenu* m_menuConstraints;
		QToolBar* m_toolbarConstraints;
		
		QAction* m_actionOrtho;
		QAction* m_actionSnapping;
		QAction* m_actionSnappingVertices;
		QAction* m_actionSnappingNearest;
		QAction* m_actionSnappingMiddle;
		QAction* m_actionSnappingCenter;

		// Declaration menu Windows
		QMenu* m_menuWindows;
		QToolBar* m_toolbarWindows;

		// Status bar
		QStatusBar* m_statusBar;
		QLabel* m_labelX, *m_labelY;
		
		// Scene
		MdiSubWindow* m_subWindow;
		Scene* m_scene;
		View* m_view;
		QList<Layer*> *m_layerList;
			
		// Combobox layers
		QComboBox* m_comboLayers;
};

#endif

