#include "gui/MdiSubWindow.h"
#include "gui/Window.h"

MdiSubWindow::MdiSubWindow(Window* parent_) : QMdiSubWindow(parent_)
{
	m_window = parent_;
	setAttribute(Qt::WA_DeleteOnClose);
}

void MdiSubWindow::closeEvent(QCloseEvent*  closeEvent_)
{
	m_window->closedWindow();
	closeEvent_->accept();
}
