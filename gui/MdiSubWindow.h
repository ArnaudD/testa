#ifndef MDISUBWINDOW_H_INCLUDED
#define MDISUBWINDOW_H_INCLUDED

#include <QtWidgets>

class Window;

class MdiSubWindow: public QMdiSubWindow
{
	Q_OBJECT

	public:
		explicit MdiSubWindow(Window*);

	protected:
		void closeEvent(QCloseEvent*);

	private:
		Window* m_window;
};
#endif

