#ifndef VIEW_H_INCLUDED
#define VIEW_H_INCLUDED

#include <QtWidgets>

class Scene;
class View : public QGraphicsView
{

	Q_OBJECT

	public:
		View(Scene*, QWidget*);
		Scene *scene();
		QComboBox *m_comboLayers();

	protected:
		void wheelEvent (QWheelEvent*);
		void mousePressEvent (QMouseEvent*);
		void mouseMoveEvent (QMouseEvent*);
		void mouseReleaseEvent (QMouseEvent*);

	private:
		bool scaleView(qreal);
		void scrollView(QPoint);

		Scene *m_scene;

		QPoint m_oldMousePos;
		bool m_dragOn;
};
#endif
