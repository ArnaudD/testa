#include "gui/Window.h"
#include "core/support/Scene.h"

Window::Window() : QMainWindow()
{
	setWindowTitle("Testa 1.1.1");
	setMouseTracking(true);
	
	m_scene = NULL;
	m_view = NULL;
	m_layerList = NULL;
	m_subWindow = NULL;

	// Initialization central area
	m_centralArea = new QMdiArea(this);
	m_centralArea->setViewMode(QMdiArea::TabbedView);
	m_centralArea->setTabsClosable(true);
	m_centralArea->setTabsMovable(true);
	setCentralWidget(m_centralArea);
	connect(m_centralArea, SIGNAL(subWindowActivated(QMdiSubWindow *)), this, SLOT(slot_subWindowActivated())); 

	// Initialization menu File
	m_menuFile = menuBar()->addMenu("File");
	m_toolbarFile = addToolBar("File");

	m_actionNew = m_menuFile->addAction("New...");
	m_toolbarFile->addAction(m_actionNew);
	m_actionNew->setIcon(QIcon(":/icons/fileNew.png"));
	connect(m_actionNew, SIGNAL(triggered()), this, SLOT(slot_newScene()));
	
	m_actionOpen = m_menuFile->addAction("Open...");
	m_toolbarFile->addAction(m_actionOpen);
	m_actionOpen->setIcon(QIcon(":/icons/fileOpen.png"));
	connect(m_actionOpen, SIGNAL(triggered()), this, SLOT(slot_fileOpen()));
	
	m_menuFile->addSeparator();
	m_toolbarFile->addSeparator();
	
	m_actionSave = m_menuFile->addAction("Save...");
	m_toolbarFile->addAction(m_actionSave);
	m_actionSave->setIcon(QIcon(":/icons/fileSave.png"));
	m_actionSave->setEnabled(false);
	connect(m_actionSave, SIGNAL(triggered()), this, SLOT(slot_fileSave()));
	
	m_actionSaveAs = m_menuFile->addAction("Save As...");
	m_toolbarFile->addAction(m_actionSaveAs);
	m_actionSaveAs->setIcon(QIcon(":/icons/fileSaveAs.png"));
	m_actionSaveAs->setEnabled(false);
	connect(m_actionSaveAs, SIGNAL(triggered()), this, SLOT(slot_fileSaveAs()));

	m_menuFile->addSeparator();
	m_toolbarFile->addSeparator();
	
	m_actionCloseAll = m_menuFile->addAction("Close all");
	m_toolbarFile->addAction(m_actionCloseAll);
	m_actionCloseAll->setIcon(QIcon(":/icons/fileCloseAll.png"));
	m_actionCloseAll->setEnabled(false);
	connect(m_actionCloseAll, SIGNAL(triggered()), this, SLOT(slot_fileCloseAll()));
	
	m_actionExit = m_menuFile->addAction("Exit");
	m_toolbarFile->addAction(m_actionExit);
	m_actionExit->setIcon(QIcon(":/icons/fileExit.png"));
	connect(m_actionExit, &QAction::triggered, qApp, &QApplication::closeAllWindows);

	// Initialization menu Layers
	m_menuLayers = menuBar()->addMenu("Layers");
	m_menuLayers->setEnabled(false);
	m_toolbarLayers = addToolBar("Layers");
	m_toolbarLayers->hide();

	m_actionNewLayer = m_menuLayers->addAction("Add a new layer...");
	m_actionNewLayer->setIcon(QIcon(":/icons/layerNew.png"));
	m_toolbarLayers->addAction(m_actionNewLayer);
	connect(m_actionNewLayer, SIGNAL(triggered()), this, SLOT(slot_newLayer()));
	
	m_actionDeleteLayer = m_menuLayers->addAction("Delete current layer");
	m_actionDeleteLayer->setIcon(QIcon(":/icons/layerDelete.png"));
	m_toolbarLayers->addAction(m_actionDeleteLayer);
	connect(m_actionDeleteLayer, SIGNAL(triggered()), this, SLOT(slot_deleteLayer()));
	
	m_menuLayers->addSeparator();
	m_toolbarLayers->addSeparator();

	m_actionRenameLayer = m_menuLayers->addAction("Rename current layer...");
	m_actionRenameLayer->setIcon(QIcon(":/icons/layerRename.png"));
	m_toolbarLayers->addAction(m_actionRenameLayer);
	connect(m_actionRenameLayer, SIGNAL(triggered()), this, SLOT(slot_nameLayer()));

	m_actionColorLayer = m_menuLayers->addAction("Color of current layer...");
	m_actionColorLayer->setIcon(QIcon(":/icons/layerColor.png"));
	m_toolbarLayers->addAction(m_actionColorLayer);
	connect(m_actionColorLayer, SIGNAL(triggered()), this, SLOT(slot_colorLayer()));
	
	m_actionToggleLayer = m_menuLayers->addAction("Toggle visibility of current layer");
	m_actionToggleLayer->setIcon(QIcon(":/icons/layerVisible.png"));
	m_toolbarLayers->addAction(m_actionToggleLayer);
	connect(m_actionToggleLayer, SIGNAL(triggered()), this, SLOT(slot_hideLayer()));
	
	m_menuLayers->addSeparator();
	m_toolbarLayers->addSeparator();
	
	m_actionRaiseLayer = m_menuLayers->addAction("Raise current layer");
	m_actionRaiseLayer->setIcon(QIcon(":/icons/layerRaise.png"));
	m_toolbarLayers->addAction(m_actionRaiseLayer);
	connect(m_actionRaiseLayer, SIGNAL(triggered()), this, SLOT(slot_upLayerLevel()));
	
	m_actionLowerLayer = m_menuLayers->addAction("Lower current layer");
	m_actionLowerLayer->setIcon(QIcon(":/icons/layerLower.png"));
	m_toolbarLayers->addAction(m_actionLowerLayer);
	connect(m_actionLowerLayer, SIGNAL(triggered()), this, SLOT(slot_downLayerLevel()));

	// Initialization du menu dessin
	m_menuDrawing = menuBar()->addMenu("Drawing");
	m_menuDrawing->setEnabled(false);
	m_toolbarDrawing = addToolBar("Drawing");
	m_toolbarDrawing->hide();

	m_actionDrawPoint = m_menuDrawing->addAction("Point");
	m_actionDrawPoint->setIcon(QIcon(":/icons/point.png"));
	m_actionDrawPoint->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawPoint);
	connect(m_actionDrawPoint, SIGNAL(triggered()), this, SLOT(slot_drawPoint()));

	m_actionDrawLine = m_menuDrawing->addAction("Line");
	m_actionDrawLine->setIcon(QIcon(":/icons/line.png"));
	m_actionDrawLine->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawLine);
	connect(m_actionDrawLine, SIGNAL(triggered()), this, SLOT(slot_drawLine()));
	
	m_actionDrawPolyline = m_menuDrawing->addAction("Polyline");
	m_actionDrawPolyline->setIcon(QIcon(":/icons/polyline.png"));
	m_actionDrawPolyline->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawPolyline);
	connect(m_actionDrawPolyline, SIGNAL(triggered()), this, SLOT(slot_drawPolyline()));
	
	m_actionDrawCircle = m_menuDrawing->addAction("Circle");
	m_actionDrawCircle->setIcon(QIcon(":/icons/circle.png"));
	m_actionDrawCircle->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawCircle);
	connect(m_actionDrawCircle, SIGNAL(triggered()), this, SLOT(slot_drawCircle()));

	m_actionDrawRect = m_menuDrawing->addAction("Rectangle");
	m_actionDrawRect->setIcon(QIcon(":/icons/rectangle.png"));
	m_actionDrawRect->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawRect);
	connect(m_actionDrawRect, SIGNAL(triggered()), this, SLOT(slot_drawRect()));

	m_actionDrawArc = m_menuDrawing->addAction("Arc");
	m_toolbarDrawing->addAction(m_actionDrawArc);
	m_actionDrawArc->setIcon(QIcon(":/icons/arc.png"));
	m_actionDrawArc->setCheckable(true);
	connect(m_actionDrawArc, SIGNAL(triggered()), this, SLOT(slot_drawArc()));

	m_actionDrawText = m_menuDrawing->addAction("Text");
	m_actionDrawText->setIcon(QIcon(":/icons/text.png"));
	m_actionDrawText->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawText);
	connect(m_actionDrawText, SIGNAL(triggered()), this, SLOT(slot_drawText()));
	
	m_actionDrawImage = m_menuDrawing->addAction("Image");
	m_actionDrawImage->setIcon(QIcon(":/icons/image.png"));
	m_actionDrawImage->setCheckable(true);
	m_toolbarDrawing->addAction(m_actionDrawImage);
	connect(m_actionDrawImage, SIGNAL(triggered()), this, SLOT(slot_drawImage()));
	
	// Initialization menu Modify
	m_menuModify = menuBar()->addMenu("Modify");
	m_menuModify->setEnabled(false);
	m_toolbarModify = addToolBar("Modify");
	m_toolbarModify->hide();
	
	m_actionSelection = m_menuModify->addAction("Selection");
	m_actionSelection->setIcon(QIcon(":/icons/selection.png"));
	m_actionSelection->setCheckable(true);
	m_toolbarModify->addAction(m_actionSelection);
	connect(m_actionSelection, SIGNAL(triggered()), this, SLOT(slot_selection()));

	m_menuModify->addSeparator();
	m_toolbarModify->addSeparator();

	m_actionTranslate = m_menuModify->addAction("Translate");
	m_actionTranslate->setIcon(QIcon(":/icons/modifyTranslate.png"));
	m_actionTranslate->setCheckable(true);
	m_toolbarModify->addAction(m_actionTranslate);
	connect(m_actionTranslate, SIGNAL(triggered()), this, SLOT(slot_translate()));

	m_actionRotate = m_menuModify->addAction("Rotate");
	m_actionRotate->setIcon(QIcon(":/icons/modifyRotate.png"));
	m_actionRotate->setCheckable(true);
	m_toolbarModify->addAction(m_actionRotate);
	connect(m_actionRotate, SIGNAL(triggered()), this, SLOT(slot_rotate()));
	
	m_actionScale = m_menuModify->addAction("Scale");
	m_actionScale->setIcon(QIcon(":/icons/modifyScale.png"));
	m_actionScale->setCheckable(true);
	m_toolbarModify->addAction(m_actionScale);
	connect(m_actionScale, SIGNAL(triggered()), this, SLOT(slot_scale()));

	m_actionCopy = m_menuModify->addAction("Copy");
	m_actionCopy->setIcon(QIcon(":/icons/modifyCopy.png"));
	m_actionCopy->setCheckable(true);
	m_toolbarModify->addAction(m_actionCopy);
	connect(m_actionCopy, SIGNAL(triggered()), this, SLOT(slot_copy()));
	
	m_menuModify->addSeparator();
	m_toolbarModify->addSeparator();
	
	m_actionCreateBlock = m_menuModify->addAction("Create block");
	m_actionCreateBlock->setIcon(QIcon(":/icons/modifyCreateBlock.png"));
	m_actionCreateBlock->setCheckable(true);
	m_toolbarModify->addAction(m_actionCreateBlock);
	connect(m_actionCreateBlock, SIGNAL(triggered()), this, SLOT(slot_createBlock()));

	m_actionExplodeBlock = m_menuModify->addAction("Explode block");
	m_actionExplodeBlock->setIcon(QIcon(":/icons/modifyExplodeBlock.png"));
	m_toolbarModify->addAction(m_actionExplodeBlock);
	connect(m_actionExplodeBlock, SIGNAL(triggered()), this, SLOT(slot_explodeBlock()));	
	
	m_menuModify->addSeparator();
	m_toolbarModify->addSeparator();

	m_actionFontType = m_menuModify->addAction("Change font...");
	m_actionFontType->setIcon(QIcon(":/icons/fontType.png"));
	m_toolbarModify->addAction(m_actionFontType);
	connect(m_actionFontType, SIGNAL(triggered()), this, SLOT(slot_fontType()));
	m_actionFontType->setEnabled(false);
	
	m_actionFontColor = m_menuModify->addAction("Change text color...");
	m_actionFontColor->setIcon(QIcon(":/icons/fontColor.png"));
	m_toolbarModify->addAction(m_actionFontColor);
	connect(m_actionFontColor, SIGNAL(triggered()), this, SLOT(slot_fontColor()));
	m_actionFontColor->setEnabled(false);

	// Initialization menu Constraints
	m_menuConstraints = menuBar()->addMenu("Constraints");
	m_menuConstraints->setEnabled(false);
	m_toolbarConstraints = addToolBar("Constraints");
	m_toolbarConstraints->hide();
	
	m_actionOrtho = m_menuConstraints->addAction("Ortho");
	m_actionOrtho->setIcon(QIcon(":/icons/orthoDisengaged.png"));
	m_actionOrtho->setCheckable(true);
	m_toolbarConstraints->addAction(m_actionOrtho);
	connect(m_actionOrtho, SIGNAL(triggered()), this, SLOT(slot_ortho()));

	m_actionSnapping = m_menuConstraints->addAction("Snapping");
	m_actionSnapping->setIcon(QIcon(":/icons/snappingEngaged.png"));
	m_actionSnapping->setCheckable(true);
	m_actionSnapping->toggle();
	
	m_toolbarConstraints->addAction(m_actionSnapping);
	connect(m_actionSnapping, SIGNAL(triggered()), this, SLOT(slot_snapping()));

	m_menuConstraints->addSeparator();
	m_toolbarConstraints->addSeparator();
	
	m_actionSnappingVertices = m_menuConstraints->addAction("Vertices");
	m_actionSnappingVertices->setIcon(QIcon(":/icons/snappingVertices.png"));
	m_actionSnappingVertices->setCheckable(true);
	m_actionSnappingVertices->toggle();
	
	m_toolbarConstraints->addAction(m_actionSnappingVertices);
	connect(m_actionSnappingVertices, SIGNAL(triggered()), this, SLOT(slot_snappingVertices()));
	
	m_actionSnappingNearest = m_menuConstraints->addAction("Nearest");
	m_actionSnappingNearest->setIcon(QIcon(":/icons/snappingNearest.png"));
	m_actionSnappingNearest->setCheckable(true);
	
	m_toolbarConstraints->addAction(m_actionSnappingNearest);
	connect(m_actionSnappingNearest, SIGNAL(triggered()), this, SLOT(slot_snappingNearest()));
	
	m_actionSnappingMiddle = m_menuConstraints->addAction("Middle");
	m_actionSnappingMiddle->setIcon(QIcon(":/icons/snappingMiddle.png"));
	m_actionSnappingMiddle->setCheckable(true);
	
	m_toolbarConstraints->addAction(m_actionSnappingMiddle);
	connect(m_actionSnappingMiddle, SIGNAL(triggered()), this, SLOT(slot_snappingMiddle()));
	
	m_actionSnappingCenter = m_menuConstraints->addAction("Center");
	m_actionSnappingCenter->setIcon(QIcon(":/icons/snappingCenter.png"));
	m_actionSnappingCenter->setCheckable(true);
	
	m_toolbarConstraints->addAction(m_actionSnappingCenter);
	connect(m_actionSnappingCenter, SIGNAL(triggered()), this, SLOT(slot_snappingCenter()));
	
	// Initialization menu Window
	m_menuWindows = menuBar()->addMenu("Windows");
	m_menuWindows->setEnabled(false);
	
	m_toolbarWindows = addToolBar("Windows");
	m_toolbarWindows->hide();

	QAction *action_Windows_cascade = m_menuWindows->addAction("Cascade");
	m_toolbarWindows->addAction(action_Windows_cascade);
	connect(action_Windows_cascade, SIGNAL(triggered()),m_centralArea, SLOT(cascadeSubWindows()));
	action_Windows_cascade->setIcon(QIcon(":/icons/windowsCascade.png"));

	QAction *action_Windows_mosaique = m_menuWindows->addAction("Mosaic");
	m_toolbarWindows->addAction(action_Windows_mosaique);
	connect(action_Windows_mosaique, SIGNAL(triggered()), m_centralArea, SLOT(tileSubWindows()));
	action_Windows_mosaique->setIcon(QIcon(":/icons/windowsMosaic.png"));

	QAction *action_Windows_precedente = m_menuWindows->addAction("Previous window");
	m_toolbarWindows->addAction(action_Windows_precedente);
	connect(action_Windows_precedente, SIGNAL(triggered()), m_centralArea, SLOT(activatePreviousSubWindow()));
	action_Windows_precedente->setIcon(QIcon(":/icons/windowsPrevious.png"));

	QAction *action_Windows_suivante = m_menuWindows->addAction("Next window");
	m_toolbarWindows->addAction(action_Windows_suivante);
	connect(action_Windows_suivante, SIGNAL(triggered()), m_centralArea, SLOT(activateNextSubWindow()));
	action_Windows_suivante->setIcon(QIcon(":/icons/windowsNext.png"));

	// Initialization ComboBox Layers
	m_comboLayers = new QComboBox(this);
	m_toolbarLayers->addWidget(m_comboLayers);
	m_comboLayers->setSizeAdjustPolicy(QComboBox::AdjustToContents);
	m_comboLayers->setInsertPolicy(QComboBox::InsertAlphabetically);
	m_comboLayers->setFont(QFont("Courier New"));
	
	m_comboLayers->setCurrentIndex(0);
	connect(m_comboLayers, SIGNAL(currentIndexChanged(QString)), this, SLOT(slot_changeLayer(QString)));

	// Initialization status bar
	m_statusBar = statusBar();
	m_labelX = new QLabel("X = 0", this);
	m_labelY = new QLabel("Y = 0", this);
	m_statusBar->addPermanentWidget(m_labelX);
	m_statusBar->addPermanentWidget(m_labelY);
}

void Window::setCoordStatusBar(qreal x_, qreal y_)
{
	m_labelX->setText("X = " + QString::number(x_));
	m_labelY->setText("Y = " + QString::number(y_));
}

void Window::slot_subWindowActivated()
{
	// slot_subWindowActivated() is call after a close event. Then we have to check if the scene still exists. 
	if(m_scene!=NULL)
	{
		// To avoid disabling the drawing while selecting an image to insert
		if(m_scene->getActiveFunction() != drawingFunction::drawImage)
		{
			m_scene->disableDrawingAll();
			m_scene->clearSelection();
		}
	}
	
	m_subWindow = dynamic_cast<MdiSubWindow*>(m_centralArea->activeSubWindow());
	
	// Check if exists
	if(m_subWindow != NULL)
	{
		m_view = qobject_cast<View *>(m_subWindow->widget());
		m_scene = m_view->scene();
		m_layerList = m_scene->getListLayers();
		updateCombo();
		
		// Update icons in toolbars
		if(m_scene->isOrthoEngaged() == true)
			m_actionOrtho->setIcon(QIcon(":/icons/orthoEngaged.png"));
		else
			m_actionOrtho->setIcon(QIcon(":/icons/orthoDisengaged.png"));
		
		// Update icons in toolbars
		if(m_scene->isSnappingEngaged() == true)
			m_actionSnapping->setIcon(QIcon(":/icons/snappingEngaged.png"));
		else
			m_actionSnapping->setIcon(QIcon(":/icons/snappingDisengaged.png"));
	}
}

void Window::slot_newScene()
{
	// If a scene already exists, stop the current drawing
	if(m_scene)
		m_scene->disableDrawingAll();
	
	// Create subWindow
	m_subWindow = new MdiSubWindow(this);
		
	// Create scene and view
	m_scene = new Scene(m_subWindow, this);
	m_view = new View(m_scene, m_subWindow);
	
	// Show the tab - call slot_subWindowActivated()
	m_subWindow->setWidget(m_view);
	m_centralArea->addSubWindow(m_subWindow);	
	m_subWindow->showMaximized();
	m_subWindow->show();
	
	m_toolbarLayers->show();
	m_toolbarDrawing->show();
	m_toolbarModify->show();
	m_toolbarConstraints->show();
	m_toolbarWindows->show();
	
	m_menuLayers->setEnabled(true);
	m_menuDrawing->setEnabled(true);
	m_menuModify->setEnabled(true);	
	m_menuConstraints->setEnabled(true);
	m_menuWindows->setEnabled(true);
	
	m_actionSave->setEnabled(true);
	m_actionSaveAs->setEnabled(true);
	m_actionCloseAll->setEnabled(true);
	
	m_actionSnapping->setChecked(true);
	
	// Set the title bar
	QString title = "Unsaved drawing " + QString::number(m_centralArea->subWindowList().size());
	m_subWindow->setWindowTitle(title);
}

void Window::slot_fileOpen()
{
	// Get filename through GUI
	QString filename = QFileDialog::getOpenFileName(this, "Open File", "", "TestaCAD drawing files (*.tml)");
    
	if(filename!="")
	{
		// Create a new empty scene
		slot_newScene();
		
		// Remove the layer 0
		m_scene->deleteItemsLayer(m_comboLayers->currentIndex());
		
		// Read the file
		if(IO::readTML(m_scene, m_view, filename)==false)
			QMessageBox::critical(this,"Critical Error","Failed to read file");
		else
		{
			// If success, update the title bar with the filename
			QFileInfo fileInfo(filename);
			m_subWindow->setWindowTitle(fileInfo.fileName());
			updateCombo();
		}
	} 
}

void Window::slot_fileSave()
{
	// If the scene doesn't have a filename recorded in, call SaveAs
	if(m_scene->getFilename() == "")
		slot_fileSaveAs();
	else
	{
		// If the scene has a filename, save the scene in it. Send back a message if fail
		if(IO::saveTML(m_scene, m_view, m_scene->getFilename())==false)
			QMessageBox::critical(this,"Critical Error","Failed to save file");
	}
}

void Window::slot_fileSaveAs()
{
	// Get a filename through GUI
	QString filename = QFileDialog::getSaveFileName(this, "Save as...", "", "TestaCAD drawing files (*.tml)");
	
	if(filename!="")
	{
		// If filename not empty, attribute it to the scene
		m_scene->setFilename(filename);
		
		// Save the scene. If fail, return error message. If success, update the title bar with the filename
		if(IO::saveTML(m_scene, m_view, filename) == false)
			QMessageBox::critical(this,"Critical Error","Failed to save file");
		else
		{
			QFileInfo fileInfo(filename);
			m_subWindow->setWindowTitle(fileInfo.fileName());
		}
	}
}

void Window::slot_fileCloseAll()
{
	// Close properly all tabs.
	foreach(QMdiSubWindow* subWindow, m_centralArea->subWindowList())
	{
		// Useful to get the scene pointer at the close event
		m_centralArea->setActiveSubWindow(subWindow);
		subWindow->close();
	}
}

void Window::closeEvent(QCloseEvent* event_)
{
	slot_fileCloseAll();
	event_->accept();
}

void Window::slot_nameLayer()
{
	m_scene->clearSelection();

	QString Layer_getName;
	bool resultat;
	bool ok;

	do{
		resultat = true;

		Layer_getName = QInputDialog::getText(this,"Rename layer","New name:",QLineEdit::Normal,m_scene->searchCurrentLayer()->getName(),&ok);

		if (ok==false)
			return;

		if (Layer_getName == "")
		{
			resultat = false;
			QMessageBox::warning(this,"Name invalid","Please enter a name, or press cancel.");
		}

		else
		{
			for(int i=0;i<m_layerList->size();i++)
			{
				if(m_layerList->at(i)->getName()==Layer_getName)
				{
					QMessageBox::warning(this,"Name invalid","This name is already used.");
					resultat=false;
				}
			}

			if(resultat==true)
			{
				m_scene->searchCurrentLayer()->setName(Layer_getName);
				updateCombo();
			}

		}
	}
	while(resultat==false);
}

void Window::slot_newLayer()
{
	m_scene->clearSelection();

	QString layerName;
	QColor couleur;
	int niveau;
	bool resultat;
	bool ok;

	do{
		resultat = true;
		layerName = QInputDialog::getText(this,"Add new layer","Layer name:",QLineEdit::Normal,"",&ok);

		if (ok==false)
			return;
			
		for(int i=0; i<m_layerList->size(); i++)
		{
			if(layerName == m_layerList->at(i)->getName())
				resultat = false;
		}

		if (layerName == "")
		{
			resultat = false;
			QMessageBox::warning(this,"Name invalid","Please enter a name, or press cancel.");
		}
		else
		{
			for(int i=0;i<m_layerList->size();i++)
			{
				if(m_layerList->at(i)->getName()==layerName)
				{
					QMessageBox::warning(this,"Name invalid","This name is already used.");
					resultat=false;
				}
			}
		}

	}
	while(resultat==false);

	// Color selection
	couleur = QColorDialog::getColor(Qt::white);
	if(couleur.isValid()==false)
		return;

	// Level selection
	niveau = QInputDialog::getInt(this,"Add new layer","Level:",0,-999,9999,1,&ok);
	if (ok==false)
		return;

	m_layerList->append(new Layer(layerName, couleur, true, niveau, false));
	updateCombo();
	
	m_comboLayers->setCurrentIndex(m_comboLayers->count()-1); // This call the slot slot_changeLayer and hence set the layer current

}

void Window::slot_changeLayer(QString newlySelectedLayerName)
{
	if(newlySelectedLayerName != "") // avoid a glitch at startup
	{
		// Unactivate current layer
		for(int i=0;i<m_layerList->size();i++)
		{
					if(m_layerList->at(i)->isCurrent()==true)
						m_layerList->at(i)->setCurrent(false);
		}

		// Activate the new one
		QStringList split=newlySelectedLayerName.split(" ",QString::SkipEmptyParts);
		m_scene->searchLayer(split[2])->setCurrent(true);
		m_scene->changeLayerSelectedItems(split[2]);
	}
}

void Window::slot_colorLayer()
{
	QColor nouvelle_couleur = QColorDialog::getColor(Qt::white);
	m_scene->searchCurrentLayer()->setColor(nouvelle_couleur);
	updateCombo();
}

void Window::slot_hideLayer()
{
	m_scene->toggleLayerVisibility();
	updateCombo();
}

void Window::slot_upLayerLevel()
{
	m_scene->searchCurrentLayer()->setLevelUp();
	updateCombo();
}

void Window::slot_downLayerLevel()
{
	m_scene->searchCurrentLayer()->setLevelDown();
	updateCombo();
}

void Window::slot_deleteLayer()
{
	if(m_comboLayers->count()>1)
	{
		if (QMessageBox::question(this,"Delete layer","Delete layer "+m_scene->searchCurrentLayer()->getName()+" and all its objects?",QMessageBox::Yes|QMessageBox::No)==QMessageBox::Yes)
		{	
			m_scene->deleteItemsLayer(m_comboLayers->currentIndex());
			
			m_comboLayers->setCurrentIndex(0);
			updateCombo();
		}
	}
	else
	{
		QMessageBox::warning(this,"Delete not allowed","You can't delete the last layer.");
	}

}

void Window::fillComboLayers()
{
	QPixmap myPixmap(10,10);
	QString state;

	for(int i=0; i<m_layerList->size(); i++)
	{
		myPixmap.fill(m_layerList->at(i)->getColor());
		if (m_layerList->at(i)->isVisible() == false)
			state = QString(QChar(0x25CB)) + " " + QString::number(m_layerList->at(i)->getLevel());
		else
			state = QString(QChar(0x25CF)) + " " + QString::number(m_layerList->at(i)->getLevel());

		int spacesToAdd = 7 - state.size();
		for(int j=0;j<spacesToAdd;j++)
		{
			state.append(" ");
		}		

		m_comboLayers->addItem(QIcon(myPixmap), state + m_layerList->at(i)->getName());
	}
}

void Window::updateCombo()
{
	m_comboLayers->clear();
	fillComboLayers();
	
	for(int i=0; i<m_layerList->size(); i++)
	{
		if(m_layerList->at(i)->isCurrent() == true)
		{
			m_comboLayers->setCurrentIndex(i);
			return;
		}
	}
}

void Window::slot_selection()
{
	if(m_actionSelection->isChecked())
		m_scene->setSceneSelection(true);
	else
		m_scene->setSceneSelection(false);
}

void Window::slot_drawPoint()
{
	if(m_actionDrawPoint->isChecked())
	{
		if(m_scene->searchCurrentLayer()->isVisible() == true)
			m_scene->setScenePoint(true);
		else
			m_scene->setScenePoint(false);
	}
	else
		m_scene->setScenePoint(false);
}

void Window::slot_drawLine()
{
	if(m_actionDrawLine->isChecked())
	{
		if(m_scene->searchCurrentLayer()->isVisible() == true)
			m_scene->setSceneLine(true);
		else
			m_scene->setSceneLine(false);
	}
	else
		m_scene->setSceneLine(false);
}

void Window::slot_drawPolyline()
{
	if(m_actionDrawPolyline->isChecked())
	{
		if(m_scene->searchCurrentLayer()->isVisible() == true)
			m_scene->setScenePolyline(true);
		else
			m_scene->setScenePolyline(false);
	}
	else
		m_scene->setScenePolyline(false);
}

void Window::slot_drawCircle()
{	
	if(m_actionDrawCircle->isChecked())
	{
		if(m_scene->searchCurrentLayer()->isVisible() == true)
			m_scene->setSceneCircle(true);
		else
			m_scene->setSceneCircle(false);
	}
	else
		m_scene->setSceneCircle(false);
}
	
void Window::slot_drawRect()
{
	if(m_actionDrawRect->isChecked())
	{
		if(m_scene->searchCurrentLayer()->isVisible() == true)
			m_scene->setSceneRectangle(true);
		else
			m_scene->setSceneRectangle(false);
	}
	else
		m_scene->setSceneRectangle(false);
}
	
void Window::slot_drawArc()
{
	if(m_actionDrawArc->isChecked())
	{
		if(m_scene->searchCurrentLayer()->isVisible() == true)
			m_scene->setSceneArc(true);
		else
			m_scene->setSceneArc(false);
	}
	else
		m_scene->setSceneArc(false);
}
	
void Window::slot_drawText()
{
	if(m_actionDrawText->isChecked())
	{
		if(m_scene->searchCurrentLayer()->isVisible() == true)
			m_scene->setSceneText(true);
		else
			m_scene->setSceneText(false);
	}
	else
		m_scene->setSceneText(false);
}

void Window::slot_drawImage()
{
	if(m_actionDrawImage->isChecked())
	{
		if(m_scene->searchCurrentLayer()->isVisible() == true)
			m_scene->setSceneImage(true);
		else
			m_scene->setSceneImage(false);
	}
	else
		m_scene->setSceneImage(false);
}

void Window::slot_fontType()
{
	QFont font_;
	foreach (QGraphicsItem *item, m_scene->items())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, get the font
				if (textItem_)
					font_ = textItem_->font();
			}
	
	bool ok;
	font_ = QFontDialog::getFont(&ok, font_, this);
	
	if(ok)
		m_scene->applyFontTexts(font_);	
}

void Window::slot_createBlock()
{
	if(m_actionCreateBlock->isChecked())
		m_scene->setSceneCreateBlock(true);
	else
		m_scene->setSceneCreateBlock(false);
}

void Window::slot_explodeBlock()
{
	m_scene->setSceneExplodeBlock();
}

void Window::slot_fontColor()
{
	QColor color_ ;
	foreach (QGraphicsItem *item, m_scene->items())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, get the color
				if (textItem_)
					color_ = textItem_->defaultTextColor();
			}
			
	color_ = QColorDialog::getColor(Qt::white, this);
	
	if(color_.isValid()==false)
		return;
	else
		m_scene->applyColorTexts(color_);	
}

void Window::slot_ortho()
{ 
	m_scene->toggleOrtho(); 
	
	if(m_scene->isOrthoEngaged() == true)
	{
		m_actionOrtho->setIcon(QIcon(":/icons/orthoEngaged.png"));
		m_actionOrtho->setChecked(true);
	}
	else
	{
		m_actionOrtho->setIcon(QIcon(":/icons/orthoDisengaged.png"));
		m_actionOrtho->setChecked(false);
	}
	
}

void Window::slot_snapping()
{
	m_scene->toggleSnapping(); 
	
	if(m_scene->isSnappingEngaged() == true)
	{
		m_actionSnapping->setIcon(QIcon(":/icons/snappingEngaged.png"));
		m_actionSnapping->setChecked(true);
		m_actionSnappingVertices->setEnabled(true);
		m_actionSnappingNearest->setEnabled(true);
	}
	else
	{
		m_actionSnapping->setIcon(QIcon(":/icons/snappingDisengaged.png"));
		m_actionSnapping->setChecked(false);
		m_actionSnappingVertices->setEnabled(false);
		m_actionSnappingNearest->setEnabled(false);
	}
	
}

void Window::slot_snappingVertices()
{
	m_scene->getSnappingNode()->snapToVertices(m_actionSnappingVertices->isChecked());
}

void Window::slot_snappingNearest()
{
	m_scene->getSnappingNode()->snapToNearest(m_actionSnappingNearest->isChecked());
}

void Window::slot_snappingMiddle()
{
	m_scene->getSnappingNode()->snapToMiddle(m_actionSnappingMiddle->isChecked());
}

void Window::slot_snappingCenter()
{
	m_scene->getSnappingNode()->snapToCenter(m_actionSnappingCenter->isChecked());
}

void Window::setEditCursor(bool status_)
{
	if(status_==true)
	m_view->setCursor(Qt::CrossCursor);
	else
	m_view->setCursor(Qt::ArrowCursor);
}

void Window::setSelectionButton(bool status_)
{
	m_actionSelection->setChecked(status_);
	setEditCursor(status_);
}

void Window::setPointButton(bool status_)
{
	m_actionDrawPoint->setChecked(status_);
	setEditCursor(status_);
}

void Window::setLineButton(bool status_)
{
	m_actionDrawLine->setChecked(status_);
	setEditCursor(status_);
}

void Window::setPolylineButton(bool status_)
{
	m_actionDrawPolyline->setChecked(status_);
	setEditCursor(status_);
}

void Window::setCircleButton(bool status_)
{
	m_actionDrawCircle->setChecked(status_);
	setEditCursor(status_);
}

void Window::setRectButton(bool status_)
{
	m_actionDrawRect->setChecked(status_);
	setEditCursor(status_);
}

void Window::setArcButton(bool status_)
{
	m_actionDrawArc->setChecked(status_);
	setEditCursor(status_);
}

void Window::setTextButton(bool status_)
{
	m_actionDrawText->setChecked(status_);
	setEditCursor(status_);
}

void Window::setImageButton(bool status_)
{
	m_actionDrawImage->setChecked(status_);
	setEditCursor(status_);
}

void Window::setTranslateButton(bool status_)
{
	m_actionTranslate->setChecked(status_);
	setEditCursor(status_);
}

void Window::setRotateButton(bool status_)
{
	m_actionRotate->setChecked(status_);
	setEditCursor(status_);
}

void Window::setScaleButton(bool status_)
{
	m_actionScale->setChecked(status_);
	setEditCursor(status_);
}

void Window::setCopyButton(bool status_)
{
	m_actionCopy->setChecked(status_);
	setEditCursor(status_);
}

void Window::setCreateBlock(bool status_)
{
	m_actionCreateBlock->setChecked(status_);
	setEditCursor(status_);
}

QString Window::openImageDialog()
{
	return QFileDialog::getOpenFileName(this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp *.tif)"));
}

void Window::closedWindow()
{
	if(m_scene != NULL)
	{
		// Check if modification and propose to save
		if(m_scene->items().size() > 1 or m_layerList->size() > 1) // there is 1 invisible item which is the snapping item
		{
			QMessageBox::StandardButton button;

			button = QMessageBox::question(this, m_subWindow->windowTitle(), "Would you like to save your work?", QMessageBox::Yes|QMessageBox::No);
		
			if (button == QMessageBox::Yes)
				slot_fileSave();
		}
	}

	// If last MdiSubWindow alive, hide toolbar and menus
	if(m_centralArea->subWindowList().size()==1)
	{
		m_toolbarLayers->hide();
		m_toolbarDrawing->hide();
		m_toolbarConstraints->hide();
		m_toolbarWindows->hide();
		
		m_menuLayers->setEnabled(false);
		m_menuDrawing->setEnabled(false);
		m_menuConstraints->setEnabled(false);
		m_menuWindows->setEnabled(false);
		
		m_actionSave->setEnabled(false);
		m_actionSaveAs->setEnabled(false);
		m_actionCloseAll->setEnabled(false);
	}
	
	// Desalocate pointers. Objects are deleted by parent subWindow
	m_scene = NULL;
	m_view = NULL;
	m_layerList = NULL;
	m_subWindow = NULL;
}

void Window::setFontActions(bool status_)
{
	m_actionFontType->setEnabled(status_);
	m_actionFontColor->setEnabled(status_);
}

void Window::keyPressEvent(QKeyEvent *e)
{
	switch (e->key())
	{
		case Qt::Key_F8:
			slot_ortho();
			break;
		case Qt::Key_F3:
			slot_snapping();
			break;
		}
		
	QMainWindow::keyPressEvent(e);
}

void Window::slot_translate()
{
	if(m_actionTranslate->isChecked())
		m_scene->setSceneTranslate(true);
	else
		m_scene->setSceneTranslate(false);
}

void Window::slot_rotate()
{
	if(m_actionRotate->isChecked())
		m_scene->setSceneRotate(true);
	else
		m_scene->setSceneRotate(false);
}

void Window::slot_scale()
{
	if(m_actionScale->isChecked())
		m_scene->setSceneScale(true);
	else
		m_scene->setSceneScale(false);
}

void Window::slot_copy()
{
	if(m_actionCopy->isChecked())
		m_scene->setSceneCopy(true);
	else
		m_scene->setSceneCopy(false);
}

