Testa
=====

Version: 1.1.1
License: GNU General Public License v3

## Introduction
Testa is a free and open CAD software, based on the Qt Graphics View framework.  
It features the following capabilities:
* Fast rendering based on OpenGL
* Dynamic zooming and panning
* Vector primitives: point, line, polyline, rectangle, circle, arc
* Formatted text and images
* Layer management
* Snapping and orthogonal constraints
* Reading and saving files in XML format

TODO:
* Shift, symmetry (planned for version 1.2)
* Objects explorer with topology checks (planned for version 1.2)
* DXF support (planned for version 1.2)
* Command Line interface
* More snapping options (intersection, nearest...)
* ...

At the beginning, Testa was not intended to be an out-of-the-box CAD software, but rather a basic brick to be integrated in any software in need of robust drawing capabilities, like engineering programs, GIS, games...  
Progressively, the implementation of features has made Testa usable as a standalone CAD software.

## Installation
### Linux (Debian, Ubuntu)
* Install Qt5 and git

```
sudo apt-get install qt5-default git
```

* Clone project

```
git clone --depth 1 https://framagit.org/ArnaudD/testa.git
```

* Move into the project's folder

```
cd testa
```

* Build

```
qmake
make
```

In case you accidentally rebuild the project file using `qmake-project`, add the following lines `QT += widgets opengl` and `CONFIG += qt` to testa.pro

* Run

```
./testa
```

### Windows
* Download and install Qt
* Clone or download the project
* In testa.pro, add `LIBS += -LC:\Qt\Qt5.12.0\5.12.0\mingw73_64\lib\libQt5OpenGL.a -lopengl32`
where the path C:\Qt\Qt5.12.0\5.12.0\mingw73_64\lib\libQt5OpenGL.a depends of your local confirguration.
* Move into the project folder in the Qt command line and build the project.
```
qmake
mingw32-make
```
* Copy-paste the requested DLLs from C:\Qt\Qt5.12.0\5.12.0\mingw73_64\bin (actually this path depends of your local configuration) to the build folder.

## Changelog
### Version 1.1.1
* Reading and saving in XML format (*.TML)  
`git clone -b v1.1.1 --depth 1 https://framagit.org/ArnaudD/testa.git`

### Version 1.1
* Blocks
* Translation, rotation, scale, copy
* Bug fixes  
`git clone -b v1.1 --depth 1 https://framagit.org/ArnaudD/testa.git`

### Version 1.0 
Initial version  
`git clone -b v1.0 --depth 1 https://framagit.org/ArnaudD/testa.git`

## Developers' corner
### Architecture
Scene is the surface where features lives. View provides the current view over the Scene, implementing zooming and panning.  
All graphics features (point, line, polyline, rectangle, circle) inherits directly from ShapeItem which inherits from QGraphicsItem. TextItem inherits both from ShapeItem and QGraphicsTextItem, which provides the text editing capabilities.  
ImageItem inherits both from ShapeItem and QGraphicsPixmapItem, which provides Pixmap handling capabilities.  
ShapeItem provides an abstraction for layer management (from class Layer) and nodes handling (from class Node).  
Class Scene3D provides OpenGl support through the viewport of QGraphicsView.  
SnappingNode is providing the on-scene snapping indicator. There is only instance per Scene.  

### Naming convention
* Variables begin with a lower-case character. If it aggregates several words, each of them begin with an upper-case character. Example: `bool isActive;`
* Classes begin with an upper-case character. Example: `class Scene {};`

Variables are identified according to their scope:
* In a class, a variable within the scope "private" is prefixed with "m_". Example: `int m_variable;`
* In a function, arguments are suffixed by "_". Example: `void myFunction(int myVariable_) {}`
* All other variables (scope restricted to a function) are name-free.

Commits should be prefixed with one of the following tags:  
BUG: bug fix  
DOC: documentation  
ENH: enhancement, new feature  
MAINT: code maintenance

## Credits
XML parser: TinyXML-2 <http://www.grinninglizard.com/tinyxml2/>

Many icons are taken or inspired from the Tangoified icons set for Inkscape and the Tango Icon Theme:  
<http://wiki.inkscape.org/wiki/index.php/TangoifiedIcons>  
<http://tango-project.org/>


## Some reading, for inspiration
*Flatland: A Romance of Many Dimensions*, by Edwin Abbott Abbott.

