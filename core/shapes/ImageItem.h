#ifndef IMAGEITEM_H_INCLUDED
#define IMAGEITEM_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"
#include "core/support/Node.h"
#include "core/support/Layer.h"

class Scene;

class ImageItem : public ShapeItem, public QGraphicsPixmapItem
{

	public:
		ImageItem(Scene*, Layer*, QPointF, QPixmap, QString);
		QString getFilename() const;
		
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();
		
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual QPointF getMiddlePos(QPointF, qreal);

	protected:
		QRectF boundingRect() const;
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
		QString m_filename;
		qreal m_scaleFactor;
};

#endif
