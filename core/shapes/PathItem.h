#ifndef PATHITEM_H_INCLUDED
#define PATHITEM_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"
#include "core/support/Node.h"
#include "core/support/Layer.h"

class Scene;

class PathItem : public ShapeItem
{
	public:
		PathItem(Scene*, Layer*, QPointF, bool, bool);
		
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();
		
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual QPointF getMiddlePos(QPointF, qreal);
		
		void lineTo(QPointF);
		void updateLastPos(QPointF);
		void updatePos(QPointF, int);
		QPainterPath getPath();
		void removeLastElement();
		void reconstructPathfromNodes();
		void reconstructNodesfromPath();
		void setPath(QPainterPath);
		
		bool getIsRectangle();
		void initRectangle(QPointF);
		void updateRectangle(QPointF, QPointF);

	protected:
		QPainterPath m_path;
		bool m_isRectangle;
		QRectF boundingRect() const;
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
};

#endif
