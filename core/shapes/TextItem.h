#ifndef TEXTITEM_H_INCLUDED
#define TEXTITEM_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"
#include "core/support/Node.h"
#include "core/support/Layer.h"

class Scene;

class TextItem : public QGraphicsTextItem, public ShapeItem
{

	public:
		TextItem(Scene*, Layer*, QPointF, QString);
		
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();
		
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual QPointF getMiddlePos(QPointF, qreal);

		bool hasCustomColor() const;
		QColor getCustomColor() const;
		QString getText() const;
		QFont getFont() const;
		
		void setCustomColor(QColor);
		void outFocus();
		
	protected:
		QRectF boundingRect() const;
		void mouseDoubleClickEvent (QGraphicsSceneMouseEvent*);
		void mousePressEvent (QGraphicsSceneMouseEvent*);
		void focusOutEvent(QFocusEvent*);
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
		void keyPressEvent(QKeyEvent *e);

	private:
		bool m_hasCustomColor;
};

#endif
