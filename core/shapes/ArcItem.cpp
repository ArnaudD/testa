#include "core/shapes/ArcItem.h"
#include "core/support/Scene.h"

ArcItem::ArcItem(Scene *scene_, Layer *layer_, QPointF p1_, QPointF p2_, QPointF p3_, bool construction_) : ShapeItem(scene_, layer_, p1_, construction_)
{
	ShapeItem::setTransform(QTransform::fromScale(1, -1));

	addNode(2); // Center
			
	addNode(4);
	addNode(4);
	addNode(4);
	
	setArc(p1_, p2_, p3_);
	
	m_type = 0;
	
	scene_->addItem(this);
}

QRectF ArcItem::boundingRect() const
{
	return m_boundingRect;
}

void ArcItem::paint(QPainter *painter_, const QStyleOptionGraphicsItem*, QWidget*)
{
	if (m_layer->isVisible() == true)
	{
		painter_->setPen(getPen());
		
		if(areAligned==true)
		{
			if(MathCoord::isBetween(m_listNodes.at(2)->pos(), m_listNodes.at(1)->pos(), m_listNodes.at(3)->pos()))
			{
				// Draw a line when the points are align in normal configuration (small arc). We have to recalculate the pos since item is scaled(1,-1)
				QPointF pos1 = m_listNodes.at(1)->pos()-pos();
				pos1.setY(pos().y()-m_listNodes.at(1)->pos().y());
				QPointF pos2 = m_listNodes.at(2)->pos()-pos();
				pos2.setY(pos().y()-m_listNodes.at(2)->pos().y());	
				QPointF pos3 = m_listNodes.at(3)->pos()-pos();
				pos3.setY(pos().y()-m_listNodes.at(3)->pos().y());			
				
				painter_->drawLine(pos1, pos2);
				painter_->drawLine(pos2, pos3);
			}
			else
			{
				// Draw a circle when the points are align in extern configuration (big arc).
				painter_->drawEllipse(QPointF(0,0),m_radius, m_radius);
			}
		}
		else
			painter_->drawArc(m_rect, m_startAngle, m_spanAngle);
	}
}

void ArcItem::setArc(QPointF pos1_, QPointF pos2_, QPointF pos3_)
{
	// Assign nodes position
	m_listNodes[1]->setPos(pos1_);
	m_listNodes[2]->setPos(pos2_);
	m_listNodes[3]->setPos(pos3_);
	
	// Intercept a divide by 0 in compute center
	if(pos1_.y() == pos2_.y() or pos2_.y() == pos3_.y())
		pos2_.setY(pos2_.y()+0.00001);

	// Compute center and radius
	QPointF center = MathCoord::centerCircle(pos1_, pos2_, pos3_);
	m_listNodes[0]->setPos(center); 
	setPos(center);
	m_radius = MathCoord::distance(m_listNodes[0]->pos(), pos1_);

	// Calculate startAngle
	qreal azCP3 = MathCoord::azimuth(center, pos3_);
	qreal azCP1 = MathCoord::azimuth(center, pos1_);
	qreal azE = M_PI_2;
	m_startAngle = fmod(azE - azCP3,2*M_PI)*180/M_PI*16;
	
	// Calculate determinant
	qreal delta = MathCoord::determinant(pos1_, pos2_, pos3_);
	
	// Determine the geometry of the arc
	qreal xMin=pos1_.x();
	qreal yMin=pos1_.y();
	qreal xMax=pos1_.x();
	qreal yMax=pos1_.y();
	
	if(delta<0) // Arc clockwise = rotating from Pos1 to Pos3
	{
		// Calculate spanAngle, eliminate the 2PI uncertainty
		m_spanAngle = azCP3-azCP1;
		
		if(m_spanAngle<0)
			m_spanAngle+=2*M_PI;
		
		// Iterate clockwise from pos1_ to pos3_ to find XY min/max to determine the bounding box
		
		// if we cross north (azimuth 0) in the iteration
		if(azCP1 > azCP3)
			azCP3+=2*M_PI;
		
		for(qreal az = azCP1; az < azCP3; az+=m_spanAngle/64)
		{

			QPointF proj = MathCoord::projectPoint(center, az, m_radius);
			
			if(proj.x()<xMin)
				xMin = proj.x();
			else if(proj.x()>xMax)
				xMax = proj.x();
				
			if(proj.y()<yMin)
				yMin = proj.y();
			else if(proj.y()>yMax)
				yMax = proj.y();
		}
	}
	else // Counter-clockwise = rotating from Pos3 to Pos1
	{
		// Calculate spanAngle, eliminate the 2PI uncertainty
		m_spanAngle = -(MathCoord::azimuth(center,pos1_)-azCP3);
		
		if(m_spanAngle>0)
			m_spanAngle-=2*M_PI;
		
		// Iterate clockwise from pos3_ to pos1_ to find XY min/max to determine the bounding box
		
		// if we cross north (azimuth 0) in the iteration
		if(azCP3 > azCP1)
			azCP1+=2*M_PI;
		
		for(qreal az = azCP3; az < azCP1; az+=-m_spanAngle/64)
		{
			QPointF proj = MathCoord::projectPoint(center, az, m_radius);
			
			if(proj.x()<xMin)
				xMin = proj.x();
			else if(proj.x()>xMax)
				xMax = proj.x();
				
			if(proj.y()<yMin)
				yMin = proj.y();
			else if(proj.y()>yMax)
				yMax = proj.y();
		}			
	}
	
	// Check if points are nearly aligned: will simulate at painting by a line (instability QPainter)
	if(m_radius > MathCoord::distance(pos1_, pos3_)*20)
		areAligned = true;
	else
		areAligned = false;
		
	// This can happen when the points are nearly aligned... we have to check manually and define the box bounding pos1_ pos3_
	// Warning: comparison of floats
	
	if(qFabs(qFabs(xMin)-qFabs(xMax))<2 or qFabs(qFabs(yMin)-qFabs(yMax))<2)
	{
		if(pos3_.x() < pos1_.x())
		{
			xMin = pos3_.x();
			xMax = pos1_.x();
		}
		else
		{
			xMin = pos1_.x();
			xMax = pos3_.x();
		}	
		if(pos3_.y() < pos1_.y())
		{
			yMin = pos3_.y();
			yMax = pos1_.y();
		}
		else
		{
			yMin = pos1_.y();
			yMax = pos3_.y();
		}
	}
	
	// Convert spanAngle in 1/16 degrees
	m_spanAngle = m_spanAngle*180/M_PI*16;
	
	// Calculate rect (bounds the whole circle) and boundingRect (bounds the arc)
	m_rect = QRectF(-m_radius, -m_radius, 2*m_radius, 2*m_radius);
	m_boundingRect = QRectF(xMin-center.x(), center.y()-yMin, xMax-xMin, yMin-yMax);	
	
	prepareGeometryChange();
}

void ArcItem::translateItem(QPointF movement_, bool ignoreFirstNode_)
{
	// To ignore the first node in the iteration since it's already moved by handling
	int i=0;
	if(ignoreFirstNode_==true)
		i=1;
	
	// Move nodes
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin()+i; iNode != m_listNodes.end(); ++iNode)
		(*iNode)->moveBy( movement_.x(), movement_.y() );
}

void ArcItem::rotateItem(QPointF origin_, qreal angle_)
{
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
	{	
		(*iNode)->rotateNode(origin_, angle_);	
	}
	
	setArc(m_listNodes.at(1)->pos(), m_listNodes.at(2)->pos(), m_listNodes.at(3)->pos());
}

void ArcItem::scaleItem(QPointF origin_, qreal factor_)
{
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
	{	
		(*iNode)->scaleNode(origin_, factor_);	
	}
	
	setArc(m_listNodes.at(1)->pos(), m_listNodes.at(2)->pos(), m_listNodes.at(3)->pos());
}

QPointF ArcItem::getNearestPos(QPointF mousePos_, qreal snappingDistance_)
{
	// Calculate azimuth from center circle to mousePos
	qreal azCMouse = MathCoord::azimuth(m_listNodes.first()->pos(), mousePos_);
	
	// Project point to circle
	QPointF snapPos = MathCoord::projectPoint(m_listNodes.first()->pos(), azCMouse, MathCoord::distance(m_listNodes.first()->pos(), m_listNodes.last()->pos()));

	// Check using the azimuth if the point is located on the arc
	qreal azCP1 = MathCoord::azimuth(m_listNodes.at(0)->pos(), m_listNodes.at(1)->pos());
	qreal azCP3 = MathCoord::azimuth(m_listNodes.at(0)->pos(), m_listNodes.at(3)->pos());
	
	// Calculate determinant
	qreal delta = MathCoord::determinant(m_listNodes.at(1)->pos(), m_listNodes.at(2)->pos(), m_listNodes.at(3)->pos());

	if(delta<0) // Arc clockwise = rotating from Pos1 to Pos3
	{
		// if we cross north (azimuth 0)
		if(azCP1 > azCP3)
		{
			if(azCMouse >=0 and azCMouse <=azCP3)
				azCMouse+=2*M_PI;
			
			azCP3+=2*M_PI;
		}
		
		if(azCMouse<azCP3 and azCMouse>azCP1 and MathCoord::distance(mousePos_, snapPos) < snappingDistance_)
			return snapPos;
	}
	else // Counter-clockwise = rotating from Pos3 to Pos1
	{
		// if we cross north (azimuth 0)
		if(azCP3 > azCP1)
		{
			if(azCMouse >=0 and azCMouse <=azCP1)
				azCMouse+=2*M_PI;

			azCP1+=2*M_PI;
		}
		
		if(azCMouse<azCP1 and azCMouse>azCP3 and MathCoord::distance(mousePos_, snapPos) < snappingDistance_)
			return snapPos;
	}
	
	return mousePos_;
}

QPointF ArcItem::getMiddlePos(QPointF mousePos_, qreal)
{
	return mousePos_;
}

void ArcItem::removeFromScene()
{
	foreach (Node *item, m_listNodes)
	{
		delete item;
	}
	
	delete this;
}

QGraphicsItem* ArcItem::clone()
{
	ArcItem* shapeCloned = new ArcItem(m_scene, m_layer, m_listNodes.at(1)->pos(), m_listNodes.at(2)->pos(), m_listNodes.at(3)->pos(), false);
	
	return dynamic_cast<QGraphicsItem*>(shapeCloned);
}
