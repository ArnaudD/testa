#ifndef ARCITEM_H_INCLUDED
#define ARCITEM_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"
#include "core/support/Node.h"
#include "core/support/Layer.h"
#include "core/support/MathCoord.h"

class Scene;

class ArcItem : public ShapeItem
{
	public:
		ArcItem(Scene*, Layer*, QPointF, QPointF, QPointF, bool);
		
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();
		
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual QPointF getMiddlePos(QPointF, qreal);
		
		void setArc(QPointF, QPointF, QPointF);
		
	protected:
		QRectF boundingRect() const;
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
		
	private:
		qreal m_radius;
		qreal m_spanAngle;
		qreal m_startAngle;
		QRectF m_rect;
		QRectF m_boundingRect;
		bool areAligned;
};

#endif
