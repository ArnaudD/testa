#ifndef BLOCKITEM_H_INCLUDED
#define BLOCKITEM_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"
#include "core/support/Node.h"
#include "core/support/Layer.h"

class Scene;

class BlockItem : public QGraphicsItemGroup, public ShapeItem
{
	public:
		BlockItem(Scene*, Layer*, QPointF, QList<QGraphicsItem*>, bool);
		
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();
		
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual QPointF getMiddlePos(QPointF, qreal);
		
		void explode();

	protected:
		QRectF boundingRect() const;
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
		QVariant itemChange(GraphicsItemChange, const QVariant&);
		void hoverEnterEvent (QGraphicsSceneHoverEvent*);
		void hoverLeaveEvent (QGraphicsSceneHoverEvent*);
				
	private:

};

#endif
