#include "core/shapes/BlockItem.h"
#include "core/support/Scene.h"

BlockItem::BlockItem(Scene *scene_, Layer *layer_, QPointF p1_, QList <QGraphicsItem*> listItems_, bool construction_) : QGraphicsItemGroup(), ShapeItem(scene_, layer_, p1_, construction_)
{
	// Multiple inheritance
	
	ShapeItem::setTransform(QTransform::fromScale(1, -1));
	
	QGraphicsItemGroup::setAcceptHoverEvents(true);
	QGraphicsItemGroup::setFlag(QGraphicsItem::ItemIsSelectable, true); 
	QGraphicsItemGroup::setFlag(QGraphicsItem::ItemIsFocusable, true); 

	addNode(6);
	m_listNodes.first()->setPos(p1_); 
	QGraphicsItemGroup::setPos(p1_);
	
	foreach (QGraphicsItem* item, listItems_)
	{
        addToGroup(item);
	}
	
	m_type = 1;

	scene_->addItem(dynamic_cast<QGraphicsItemGroup*>(this));
}


QVariant BlockItem::itemChange(GraphicsItemChange change_, const QVariant &data_ )
{
	if( change_ == QGraphicsItem::ItemSelectedChange )
	{
		if(data_ == true)
		{
			foreach (Node *node, m_listNodes)
				node->show();
		}
		else
		{
			foreach (Node *node, m_listNodes)
				node->hide();
		}
	}

	return QGraphicsItemGroup::itemChange(change_, data_);
}

QRectF BlockItem::boundingRect() const
{
	return QGraphicsItemGroup::boundingRect();
}

void BlockItem::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{
}

void BlockItem::hoverEnterEvent (QGraphicsSceneHoverEvent* event_)
{
	foreach(QGraphicsItem* item, QGraphicsItemGroup::childItems())
		{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			shape->setMouseIsOver(true);
			QGraphicsItemGroup::update();
		}
	
	ShapeItem::hoverEnterEvent(event_);
}

void BlockItem::hoverLeaveEvent (QGraphicsSceneHoverEvent* event_)
{
		foreach(QGraphicsItem* item, QGraphicsItemGroup::childItems())
		{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			shape->setMouseIsOver(false);
			QGraphicsItemGroup::update();
		}
	
	ShapeItem::hoverLeaveEvent(event_);
}

void BlockItem::translateItem(QPointF movement_, bool ignoreFirstNode_)
{
	// To ignore the first node in the iteration since it's already moved by handling
	if(ignoreFirstNode_==false)
		m_listNodes.at(0)->moveBy( movement_.x(), movement_.y() );
		
	// Move child's nodes	
	foreach(QGraphicsItem* item, QGraphicsItemGroup::childItems())
	{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			shape->translateItem(movement_, false);							
	}
	
	// The items are moved by the parent's block translation. No need of explicit move.
}

void BlockItem::rotateItem(QPointF origin_, qreal angle_)
{
	m_listNodes.at(0)->rotateNode(origin_, angle_);	
	QGraphicsItemGroup::setPos(m_listNodes.at(0)->pos());
	
	// Rotate child's nodes	
	foreach(QGraphicsItem* item, QGraphicsItemGroup::childItems())
	{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			shape->rotateItem(origin_, angle_);
			
			if(QGraphicsItemGroup::pos() != origin_) // trick to handle rotation of blocks in the rotation block
				shape->setPos( shape->getListNodes().at(0)->pos() - origin_ - QGraphicsItemGroup::pos());
			else
				shape->setPos( shape->getListNodes().at(0)->pos() - QGraphicsItemGroup::pos());
	}
}

void BlockItem::scaleItem(QPointF origin_, qreal factor_)
{
	m_listNodes.at(0)->scaleNode(origin_, factor_);	
	QGraphicsItemGroup::setPos(m_listNodes.at(0)->pos());
	
	// Scale child's nodes	
	foreach(QGraphicsItem* item, QGraphicsItemGroup::childItems())
	{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			shape->scaleItem(origin_, factor_);
			
			if(QGraphicsItemGroup::pos() != origin_) // trick to handle scale of blocks in the scale block
				shape->setPos( shape->getListNodes().at(0)->pos() - origin_ - QGraphicsItemGroup::pos());
			else
				shape->setPos( shape->getListNodes().at(0)->pos() - QGraphicsItemGroup::pos());
	}
}

void BlockItem::explode()
{
		foreach(QGraphicsItem* item, QGraphicsItemGroup::childItems())
		{
			removeFromGroup(item);
		}
}

QPointF BlockItem::getNearestPos(QPointF mousePos_, qreal)
{
	return mousePos_;
}

QPointF BlockItem::getMiddlePos(QPointF mousePos_, qreal)
{
	return mousePos_;
}


void BlockItem::removeFromScene()
{
	delete m_listNodes.at(0);
	
	// Move child's nodes	
	foreach(QGraphicsItem* item, QGraphicsItemGroup::childItems())
	{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			shape->removeFromScene();							
	}	
	
	delete this;
}

QGraphicsItem* BlockItem::clone()
{
	QList<QGraphicsItem*> list;
	
	foreach(QGraphicsItem* item, QGraphicsItemGroup::childItems())
	{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			list.append(shape->clone());							
	}	
	
	BlockItem* shapeCloned = new BlockItem(m_scene, m_layer, m_listNodes.first()->pos(), list, getIsConstructing());
	
	return dynamic_cast<QGraphicsItem*>(dynamic_cast<QGraphicsItemGroup*>(shapeCloned));		
}
