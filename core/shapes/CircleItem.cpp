#include "core/shapes/CircleItem.h"
#include "core/support/Scene.h"

CircleItem::CircleItem(Scene *scene_, Layer *layer_, QPointF p1_, QPointF p2_, bool construction_) : ShapeItem(scene_, layer_, p1_, construction_)
{
	addNode(2);
	addNode(3);

	m_listNodes.first()->setPos(p1_); 
	m_listNodes.last()->setPos(p2_); 
	
	m_type = 2;
	
	scene_->addItem(this);
}

QRectF CircleItem::boundingRect() const
{
	QLineF radius = QLineF(m_listNodes.first()->pos(),m_listNodes.last()->pos());
	return QRectF(-radius.length(), -radius.length(), radius.length()*2, radius.length()*2).normalized();
}

void CircleItem::paint(QPainter *painter_, const QStyleOptionGraphicsItem*, QWidget*)
{
	if (m_layer->isVisible() == true)
	{
		painter_->setPen(getPen());
		QLineF radius = QLineF(m_listNodes.first()->pos(), m_listNodes.last()->	pos());
		painter_->drawEllipse(QPointF(0,0),radius.length(), radius.length());
	}
}

void CircleItem::setCircle(QPointF p1_, QPointF p2_)
{
	m_listNodes.first()->setPos(p1_);
	m_listNodes.last()->setPos(p2_);

	prepareGeometryChange();
}

void CircleItem::translateItem(QPointF movement_, bool ignoreFirstNode_)
{
	// To ignore the first node in the iteration since it's already moved by handling
	int i=0;
	if(ignoreFirstNode_==true)
		i=1;
	
	// Move nodes
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin()+i; iNode != m_listNodes.end(); ++iNode)
		(*iNode)->moveBy( movement_.x(), movement_.y() );
}

void CircleItem::rotateItem(QPointF origin_, qreal angle_)
{
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
	{	
		(*iNode)->rotateNode(origin_, angle_);	
	}
}

void CircleItem::scaleItem(QPointF origin_, qreal factor_)
{
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
	{	
		(*iNode)->scaleNode(origin_, factor_);	
	}
}

QPointF CircleItem::getNearestPos(QPointF mousePos_, qreal snappingDistance_)
{
	// Calculate azimuth from center circle to mousePos
	qreal azCMouse = MathCoord::azimuth(m_listNodes.first()->pos(), mousePos_);
	
	// Project point to circle
	QPointF snapPos_ = MathCoord::projectPoint(m_listNodes.first()->pos(), azCMouse, MathCoord::distance(m_listNodes.first()->pos(), m_listNodes.last()->pos()));
	
	if(MathCoord::distance(mousePos_, snapPos_) < snappingDistance_)
		return snapPos_;

	return mousePos_;
}

QPointF CircleItem::getMiddlePos(QPointF mousePos_, qreal)
{
	return mousePos_;
}

void CircleItem::removeFromScene()
{
	foreach (Node *item, m_listNodes)
	{
		delete item;
	}
	
	delete this;
}

QGraphicsItem* CircleItem::clone()
{
	CircleItem* shapeCloned = new CircleItem(m_scene, m_layer, m_listNodes.first()->pos(), m_listNodes.last()->pos(), false);
	
	return dynamic_cast<QGraphicsItem*>(shapeCloned);
}
