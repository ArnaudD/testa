#include "core/shapes/ShapeItem.h"
#include "core/support/Scene.h"

ShapeItem::ShapeItem(Scene* scene_, Layer* layer_, QPointF p1_, bool construction_) : QGraphicsItem()
{
	setAcceptHoverEvents(true);
	setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsFocusable);

	m_scene = scene_;
	m_layer = layer_;
	m_isConstructing = construction_;
	m_mouseIsOver = false;
	
	setPos(p1_);
}

ShapeItem::ShapeItem()
{
	setAcceptHoverEvents(true);
	setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsFocusable);
}

Layer* ShapeItem::getLayer() const
{
	return m_layer;
}

Scene* ShapeItem::getScene() const
{
	return m_scene;
}

int ShapeItem::getType() const
{
	return m_type;
}

QList<Node*> ShapeItem::getListNodes() const
{
	return m_listNodes;
}

void ShapeItem::addNode(int role_)
{
	Node *node = new Node(m_scene, this, role_, &m_listNodes);
	m_listNodes.append(node);
	m_scene->addItem(node);
}

void ShapeItem::setLayer(Layer *layer_)
{
	m_layer = layer_;
}

QPointF ShapeItem::getClosestNodePos(QPointF pos_, qreal snappingDistance_)
{
	foreach (Node *Node, m_listNodes)
	{
		if(Node->getRole() != 2)
		{
			if(  abs(Node->pos().x()-pos_.x()) < snappingDistance_ and abs(Node->pos().y()-pos_.y()) < snappingDistance_ )
				return Node->pos();
		}
	}
	return pos_;
}

QPointF ShapeItem::getNearestPos(QPointF, qreal)
{
	return QPointF(0,0);
}

QPointF ShapeItem::getMiddlePos(QPointF, qreal)
{
	return QPointF(0,0);
}

QPointF ShapeItem::getCenterNodePos(QPointF pos_, qreal snappingDistance_)
{
	foreach (Node *Node, m_listNodes)
	{
		if(Node->getRole() == 2)
		{
			if(  abs(Node->pos().x()-pos_.x()) < snappingDistance_ and abs(Node->pos().y()-pos_.y()) < snappingDistance_ )
			return Node->pos();
		}
	}
	return pos_;
}

QVariant ShapeItem::itemChange(GraphicsItemChange change_, const QVariant &data_ )
{
	if( change_ == QGraphicsItem::ItemSelectedChange )
	{
		if(data_ == true)
		{
			foreach (Node *node, m_listNodes)
				node->show();
		}
		else
		{
			foreach (Node *node, m_listNodes)
				node->hide();
		}
	}

	return QGraphicsItem::itemChange(change_, data_);
}

void ShapeItem::setIsConstructing(bool value_)
{
	m_isConstructing = value_;
}

bool ShapeItem::getIsConstructing() const
{
	return m_isConstructing;
}

QPen ShapeItem::getPen()
{
	QColor color = m_layer->getColor();
	setZValue(m_layer->getLevel());
	
	if(m_isConstructing == true)
		return QPen(color,0,Qt::SolidLine);
	
	if (isSelected() == false)
	{
		if (m_mouseIsOver == false)
			return QPen(color,0,Qt::SolidLine);
		else // m_mouseIsOver==true
			return QPen(color,0,Qt::DotLine);
	}
	else if (isSelected() == true)
			return QPen(color,0,Qt::DashLine);
			
	return QPen();
}

void ShapeItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event_)
{
	m_mouseIsOver = true;
	update();
	
	QGraphicsItem::hoverEnterEvent(event_);
}

void ShapeItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event_)
{
	m_mouseIsOver = false;
	update();
	
	QGraphicsItem::hoverLeaveEvent(event_);
}

void ShapeItem::setMouseIsOver(bool mouse_)
{
	m_mouseIsOver = mouse_;
	
	// Update the childs if this ShapeItem is a BlockItem too
	foreach(QGraphicsItem* item, childItems())
	{
			ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
			shape->setMouseIsOver(mouse_);
	}
	
	update();
}

void ShapeItem::removeFromScene()
{
	// Necessary to delete the nodes out of the destructors, otherwhise when closing the scene the nodes might be already destroyed automatically
}

void ShapeItem::translateItem(QPointF, bool)
{
	// move nodes (moving item is handled by QGraphicsItemGroup)
}

void ShapeItem::rotateItem(QPointF, qreal)
{
	// rotate item and nodes
}

void ShapeItem::scaleItem(QPointF, qreal)
{
	// Scale item and nodes
}

QGraphicsItem* ShapeItem::clone()
{
	// return a pointer toward a copy of the instance
	return 0;
}
