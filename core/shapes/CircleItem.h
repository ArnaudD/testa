#ifndef CIRCLEITEM_H_INCLUDED
#define CIRCLEITEM_H_INCLUDED

#include <QtWidgets>
#include "core/shapes/ShapeItem.h"
#include "core/support/Node.h"
#include "core/support/Layer.h"

class Scene;

class CircleItem : public ShapeItem
{
	public:
		CircleItem(Scene*, Layer*, QPointF, QPointF, bool);
		
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();
		
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual QPointF getMiddlePos(QPointF, qreal);
		
		void setCircle(QPointF, QPointF);

	protected:
		QRectF boundingRect() const;
		void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*);
};

#endif
