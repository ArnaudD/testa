#include "core/shapes/PathItem.h"
#include "core/support/Scene.h"

PathItem::PathItem(Scene *scene_, Layer *layer_, QPointF p1_, bool isRectangle_, bool construction_) : ShapeItem(scene_, layer_, p1_, construction_)
{
	m_isRectangle = isRectangle_;
	
	addNode(1);
	m_listNodes.first()->setPos(p1_);
	
	if(isRectangle_==false)
	m_type = 4;
	else
	m_type = 5;
	
	scene_->addItem(this);
}

QRectF PathItem::boundingRect() const
{
	return 	m_path.boundingRect();
}

void PathItem::paint(QPainter *painter_, const QStyleOptionGraphicsItem*, QWidget*)
{
	if (m_layer->isVisible() == true)
	{
		painter_->setPen(getPen());
		painter_->drawPath(m_path);
	}
}

void PathItem::lineTo(QPointF p1_)
{
	// Transfer point in item's coordinate system
	p1_ -= pos();
	m_path.lineTo(p1_);

	prepareGeometryChange();
}

void PathItem::updateLastPos(QPointF p1_)
{
	// Transfer point in item's coordinate system
	p1_ -= pos();
	m_path.setElementPositionAt(m_path.elementCount()-1, p1_.x(), p1_.y());
	
	prepareGeometryChange();	
}

void PathItem::updatePos(QPointF p1_, int i)
{
	// Transfer point in item's coordinate system
	p1_ -= pos();
	m_path.setElementPositionAt(i, p1_.x(), p1_.y());
}

void PathItem::removeLastElement()
{
	// Remove the last element in the QPainterPath. Useful when drawing a polyline and finishing with right-clic
	
	QPainterPath newPath;
	QPainterPath::Element element;
	
	for(int i=0; i<m_path.elementCount()-1; i++)
	{
		element = m_path.elementAt(i);
		
		if(element.isMoveTo() == true)
			newPath.moveTo(element.x, element.y);
		else if(element.isLineTo() == true)
			newPath.lineTo(element.x, element.y);
	}
	
	m_path = newPath;
}

// Useful when a node is moved by handling
void PathItem::reconstructPathfromNodes()
{
	setPos(m_listNodes.first()->pos());

	// Iteration through nodes, ignoring the 1st one
	for(int i=1; i<m_listNodes.count(); i++)
		m_path.setElementPositionAt(i, m_listNodes.at(i)->pos().x()-m_listNodes.first()->pos().x(), m_listNodes.at(i)->pos().y()-m_listNodes.first()->pos().y());

	prepareGeometryChange();
}

// Useful when copying the object
void PathItem::reconstructNodesfromPath()
{	
	QPointF posNode;
	
	// Ignore the first one, it's a moveTo(0,0) and we already added the node above
	for(int i=1; i<m_path.elementCount(); i++)
	{
		if(m_isRectangle == true and i==4) // don't add the last node if PathItem is a rectangle
			return;
			
		addNode(1);
		posNode.setX(pos().x() + m_path.elementAt(i).x);
		posNode.setY(pos().y() + m_path.elementAt(i).y);
		m_listNodes.last()->setPos(posNode);
	}
}

QPainterPath PathItem::getPath()
{
	return m_path;
}

// Useful when copying the object. Assuming there is no other nodes than the first one
void PathItem::setPath(QPainterPath path_)
{
	m_path = path_;
	reconstructNodesfromPath();
}

void PathItem::translateItem(QPointF movement_, bool)
{
	// Move nodes
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
		(*iNode)->moveBy( movement_.x(), movement_.y() );
}

void PathItem::rotateItem(QPointF origin_, qreal angle_)
{
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
	{	
		(*iNode)->rotateNode(origin_, angle_);	
	}
	
	reconstructPathfromNodes();
}

void PathItem::scaleItem(QPointF origin_, qreal factor_)
{
	QList<Node*>::iterator iNode;
	for (iNode = m_listNodes.begin(); iNode != m_listNodes.end(); ++iNode)
	{	
		(*iNode)->scaleNode(origin_, factor_);	
	}
	
	reconstructPathfromNodes();
}

bool PathItem::getIsRectangle()
{
	return m_isRectangle;
}

void PathItem::initRectangle(QPointF pos2_)
{
	QPointF delta = pos2_ - pos();
	
	QPointF lr = QPointF(pos().x() + delta.x(), pos().y());
	
	lineTo(lr);
	lineTo(pos2_);	

	lr = QPointF(pos().x(), pos().y() + delta.y());

	lineTo(lr);
	lineTo(pos());
}

void PathItem::updateRectangle(QPointF pos1_, QPointF pos2_)
{
	QPointF lr = QPointF(pos2_.x(), pos1_.y());
	QPointF ul = QPointF(pos1_.x(), pos2_.y());
	
	updatePos(lr, 1);
	updatePos(pos2_, 2);
	updatePos(ul, 3);
	
	prepareGeometryChange();
}

QPointF PathItem::getNearestPos(QPointF mousePos_, qreal snappingDistance_)
{
	// For each segment
	for(int i=1; i<m_path.elementCount(); i++)
	{
		QPointF A = mapToScene(QPointF(m_path.elementAt(i-1).x, m_path.elementAt(i-1).y));
		QPointF B = mapToScene(QPointF(m_path.elementAt(i).x, m_path.elementAt(i).y));

		// Linear equation ax + by + c = 0 
		qreal a = B.y() - A.y();
		qreal b = A.x() - B.x();
		qreal c = B.x()*A.y() - A.x()*B.y();
		
		// Orthogonal projection
		QPointF H;
		qreal denom = a*a+b*b;
		
		// Particular case: vertical line
		if(b==0)
		{
			H.setX(A.x());
			H.setY(mousePos_.y());
		}
		// Particular case: horizontal line
		else if(a==0)
		{
			H.setX(mousePos_.x());
			H.setY(A.y());
		}
		else
		{
			H.setX( (b*(b*mousePos_.x() - a*mousePos_.y())-a*c)/denom);
			H.setY( (a*(-b*mousePos_.x() + a*mousePos_.y())-b*c)/denom);
		}
		
		// If the projected point is in the segment and distance to mousePos is below the snapping distance
		if(MathCoord::isBetween(H, A, B) and MathCoord::distance(mousePos_, H) < snappingDistance_)
			return H;
	}
	
	return mousePos_;
}

QPointF PathItem::getMiddlePos(QPointF mousePos_, qreal snappingDistance_)
{
	// For each segment
	for(int i=1; i<m_path.elementCount(); i++)
	{
		QPointF A = mapToScene(QPointF(m_path.elementAt(i-1).x, m_path.elementAt(i-1).y));
		QPointF B = mapToScene(QPointF(m_path.elementAt(i).x, m_path.elementAt(i).y));

		// Calculate middle point
		QPointF M;
		M.setX((A.x() + B.x()) / 2);
		M.setY((A.y() + B.y()) / 2);
		
		// If the middle point is in the segment and distance to mousePos is below the snapping distance
		if(MathCoord::distance(mousePos_, M) < snappingDistance_)
			return M;
	}
	
	return mousePos_;
}

void PathItem::removeFromScene()
{
	foreach (Node *item, m_listNodes)
	{
		delete item;
	}
	
	delete this;
}

QGraphicsItem* PathItem::clone()
{
	PathItem* shapeCloned = new PathItem(m_scene, m_layer, m_listNodes.first()->pos(), getIsRectangle(), false);
	shapeCloned->setPath(m_path);
	
	return dynamic_cast<QGraphicsItem*>(shapeCloned);
}
