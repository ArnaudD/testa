#include "core/shapes/TextItem.h"
#include "core/support/Scene.h"

TextItem::TextItem(Scene *scene_, Layer *layer_, QPointF p1_, QString string_) : QGraphicsTextItem(string_), ShapeItem(scene_, layer_, p1_, false)
{
	ShapeItem::setTransform(QTransform::fromScale(1, -1));

	addNode(0);
	m_listNodes.first()->setPos(p1_); 
	
	m_hasCustomColor = false;
	
	m_type = 7;
	
	scene_->addItem(dynamic_cast<ShapeItem*>(this));
}

QRectF TextItem::boundingRect() const
{
	QTextDocument doc_;
	doc_.setHtml(QGraphicsTextItem::toHtml());
	doc_.setDefaultFont(QGraphicsTextItem::font());
	
	return QRectF(QPointF(0,0),doc_.size());
}

QString TextItem::getText() const
{
	return QGraphicsTextItem::toPlainText();
}

bool TextItem::hasCustomColor() const
{
	return m_hasCustomColor;
}

QColor TextItem::getCustomColor() const
{
	return defaultTextColor();
}

QFont TextItem::getFont() const
{
	return font();
}

void TextItem::paint(QPainter *painter_, const QStyleOptionGraphicsItem* option_, QWidget* widget_)
{
	if (m_layer->isVisible() == true)
	{
		if(m_hasCustomColor == false)
			setDefaultTextColor(m_layer->getColor());
		
		QGraphicsTextItem::paint(painter_, option_, widget_);
	}
}

void TextItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event_)
{
	if(textInteractionFlags() == Qt::TextEditorInteraction)
        QGraphicsTextItem::mouseDoubleClickEvent(event_);
	else
	{
		    setTextInteractionFlags(Qt::TextEditorInteraction);
			QGraphicsTextItem::mouseDoubleClickEvent(event_);	
	}
	
	return;
}

void TextItem::mousePressEvent (QGraphicsSceneMouseEvent *event_)
{
	if(ShapeItem::isSelected()==false)
		ShapeItem::mousePressEvent(event_);
	else
		QGraphicsTextItem::mousePressEvent(event_);
}

void TextItem::focusOutEvent(QFocusEvent *event_)
{
	outFocus();

	ShapeItem::focusOutEvent(event_);
}

void TextItem::keyPressEvent(QKeyEvent *event_)
{
	QGraphicsTextItem::keyPressEvent(event_);
}

void TextItem::setCustomColor(QColor color_)
{
	m_hasCustomColor = true;
	setDefaultTextColor(color_);
}

void TextItem::outFocus()
{
	QGraphicsTextItem::setTextInteractionFlags(Qt::NoTextInteraction);
	
	QTextCursor _cursor = textCursor();
    _cursor.clearSelection();
    setTextCursor(_cursor);
	
	ShapeItem::setFlag(QGraphicsItem::ItemIsSelectable, true);
}

void TextItem::translateItem(QPointF movement_, bool)
{
	m_listNodes.at(0)->moveBy( movement_.x(), movement_.y() );
}

void TextItem::rotateItem(QPointF origin_, qreal angle_)
{
	m_listNodes.at(0)->rotateNode(origin_, angle_);	
	QGraphicsTextItem::setPos(m_listNodes.at(0)->pos());
	
	ShapeItem::setRotation(ShapeItem::rotation() + angle_*180/M_PI);
}

void TextItem::scaleItem(QPointF origin_, qreal factor_)
{
	m_listNodes.at(0)->scaleNode(origin_, factor_);	
	QGraphicsTextItem::setPos(m_listNodes.at(0)->pos());
	
	// Scale the font
	QFont fontScale = font();
	
	// If size defined in points
	fontScale.setPointSizeF(fontScale.pointSizeF()*factor_);

	setFont(fontScale);	
}

QPointF TextItem::getNearestPos(QPointF mousePos_, qreal)
{
	return mousePos_;
}

QPointF TextItem::getMiddlePos(QPointF mousePos_, qreal)
{
	return mousePos_;
}

void TextItem::removeFromScene()
{
	// Remove from scene only if it's not in edition
	if(textInteractionFlags() != Qt::TextEditorInteraction)
	{
		delete m_listNodes.at(0);
		delete this;
	}
}

QGraphicsItem* TextItem::clone()
{
	TextItem* shapeCloned = new TextItem(m_scene, m_layer, m_listNodes.first()->pos(), QGraphicsTextItem::toPlainText());
	
	if(m_hasCustomColor)
		shapeCloned->setCustomColor(defaultTextColor());
		
	shapeCloned->setFont(font());
	shapeCloned->ShapeItem::setRotation(ShapeItem::rotation());
	
	return dynamic_cast<QGraphicsItem*>(dynamic_cast<ShapeItem*>(shapeCloned));
}
