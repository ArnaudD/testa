#ifndef SHAPEITEM_H_INCLUDED
#define SHAPEITEM_H_INCLUDED

#include <QtWidgets>
#include "core/support/Layer.h"
#include "core/support/Node.h"

class Scene;
class BlockItem;

class ShapeItem : public QGraphicsItem
{

	public:
		ShapeItem(Scene*, Layer*, QPointF, bool);
		ShapeItem();

		// Handling
		virtual void translateItem(QPointF, bool);
		virtual void rotateItem(QPointF, qreal);
		virtual void scaleItem(QPointF, qreal);
		virtual QGraphicsItem* clone();
		virtual void removeFromScene();

		// Misc
		void setLayer(Layer*);
		Layer *getLayer() const;
		Scene *getScene() const;
		int getType() const;
		
		// Snapping
		QPointF getClosestNodePos(QPointF, qreal);
		virtual QPointF getNearestPos(QPointF, qreal);
		virtual QPointF getMiddlePos(QPointF, qreal);
		QPointF getCenterNodePos(QPointF, qreal);
		
		// Construction
		void addNode(int);
		QList<Node*> getListNodes() const;
		
		// Drawing
		void setIsConstructing(bool);
		bool getIsConstructing() const;
		void setMouseIsOver(bool);
		QPen getPen();
		
	protected:
		void hoverEnterEvent (QGraphicsSceneHoverEvent*);
		void hoverLeaveEvent (QGraphicsSceneHoverEvent*);
		QVariant itemChange(GraphicsItemChange, const QVariant&);
		
		Layer *m_layer;
		Scene *m_scene;
		QList<Node*> m_listNodes;
		bool m_isConstructing;
		bool m_mouseIsOver;
		int m_type;
};

#endif
