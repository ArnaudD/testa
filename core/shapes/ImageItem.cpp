#include "core/shapes/ImageItem.h"
#include "core/support/Scene.h"

ImageItem::ImageItem(Scene *scene_, Layer *layer_, QPointF p1_, QPixmap pixmap_, QString filename_) : ShapeItem(scene_, layer_, p1_, false), QGraphicsPixmapItem(pixmap_)
{
	addNode(0);
	m_listNodes.first()->setPos(p1_);
	
	// Copy of filename is used when saving
	m_filename = filename_;
	
	m_type = 3;
	
	scene_->addItem(dynamic_cast<ShapeItem*>(this));
}

QString ImageItem::getFilename() const
{
	return m_filename;
}

QRectF ImageItem::boundingRect() const
{
	return QRectF(QPointF(0,0),QGraphicsPixmapItem::pixmap().size());
}

void ImageItem::paint(QPainter *painter_, const QStyleOptionGraphicsItem* option_, QWidget* widget_)
{
	if (m_layer->isVisible() == true)
		QGraphicsPixmapItem::paint(painter_, option_, widget_);
}

void ImageItem::translateItem(QPointF movement_, bool)
{
	m_listNodes.at(0)->moveBy( movement_.x(), movement_.y() );
}

void ImageItem::rotateItem(QPointF origin_, qreal angle_)
{
	m_listNodes.at(0)->rotateNode(origin_, angle_);	
	QGraphicsPixmapItem::setPos(m_listNodes.at(0)->pos());
	
	ShapeItem::setRotation(ShapeItem::rotation() - angle_*180/M_PI);
}

void ImageItem::scaleItem(QPointF origin_, qreal factor_)
{
	m_listNodes.at(0)->scaleNode(origin_, factor_);
	QGraphicsPixmapItem::setPos(m_listNodes.at(0)->pos());
		
	ShapeItem::setScale(factor_*ShapeItem::scale());
}

QPointF ImageItem::getNearestPos(QPointF mousePos_, qreal)
{
	return mousePos_;
}

QPointF ImageItem::getMiddlePos(QPointF mousePos_, qreal)
{
	return mousePos_;
}

void ImageItem::removeFromScene()
{
	delete m_listNodes.at(0);
	delete this;
}

QGraphicsItem* ImageItem::clone()
{
	ImageItem* shapeCloned = new ImageItem(m_scene, m_layer, m_listNodes.first()->pos(), QGraphicsPixmapItem::pixmap(), m_filename);
	shapeCloned->ShapeItem::setRotation(ShapeItem::rotation());
	shapeCloned->ShapeItem::setScale(ShapeItem::scale());
	
	return dynamic_cast<QGraphicsItem*>(dynamic_cast<ShapeItem*>(shapeCloned));
}
