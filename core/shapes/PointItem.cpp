#include "core/shapes/PointItem.h"
#include "core/support/Scene.h"

PointItem::PointItem(Scene *scene_, Layer *layer_, QPointF p1_) : ShapeItem(scene_, layer_, p1_, false)
{
	addNode(0);
	m_listNodes.first()->setPos(p1_); 
	
	m_type = 6;
	
	scene_->addItem(this);
}

QRectF PointItem::boundingRect() const
{
	return QRectF(-6,-6,12,12);
}

void PointItem::paint(QPainter *painter_, const QStyleOptionGraphicsItem*, QWidget*)
{
	double scaleValue = scale();
	double scaleX = painter_->transform().m11();
	setScale(scaleValue / scaleX);
	
	if (m_layer->isVisible() == true)
	{
		painter_->setPen(getPen());
		if(isSelected())
		{
			painter_->drawLine(-7,-7,7,7);
			painter_->drawLine(-7,7,7,-7);
		}
		else
		{
			painter_->drawLine(-3,-3,3,3);
			painter_->drawLine(-3,3,3,-3);
		}
	}
}

void PointItem::translateItem(QPointF movement_, bool)
{
	m_listNodes.at(0)->moveBy( movement_.x(), movement_.y() );
}

void PointItem::rotateItem(QPointF origin_, qreal angle_)
{
	m_listNodes.at(0)->rotateNode(origin_, angle_);	
	setPos(m_listNodes.at(0)->pos());
}

void PointItem::scaleItem(QPointF origin_, qreal factor_)
{
	m_listNodes.at(0)->scaleNode(origin_, factor_);	
	setPos(m_listNodes.at(0)->pos());
}

QPointF PointItem::getNearestPos(QPointF mousePos_, qreal)
{
	return mousePos_;
}

QPointF PointItem::getMiddlePos(QPointF mousePos_, qreal)
{
	return mousePos_;
}

void PointItem::removeFromScene()
{
	delete m_listNodes.at(0);
	delete this;
}

QGraphicsItem* PointItem::clone()
{
	PointItem* shapeCloned = new PointItem(m_scene, m_layer, m_listNodes.first()->pos());
	
	return dynamic_cast<QGraphicsItem*>(shapeCloned);
}
