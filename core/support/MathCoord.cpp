#include "core/support/MathCoord.h"

qreal MathCoord::angle(QPointF a_, QPointF b_, QPointF c_)
{
    QPointF ab = QPointF(b_.x() - a_.x(), b_.y() - a_.y());
    QPointF cb = QPointF(b_.x() - c_.x(), b_.y() - c_.y());

    qreal dot = ab.x() * cb.x() + ab.y() * cb.y(); 	// dot product
    qreal cross = ab.x() * cb.y() - ab.y() * cb.x(); // cross product

    qreal alpha = qAtan2(cross, dot);
		
    return alpha;
}

qreal MathCoord::distance(QPointF pos1_, QPointF pos2_)
{
	return qSqrt(pow(pos2_.x()-pos1_.x(),2)+pow(pos2_.y()-pos1_.y(),2));
}

qreal MathCoord::azimuth(QPointF pos1_, QPointF pos2_)
{
	QPointF delta = pos2_ - pos1_;
	
	qreal az = 2 * qAtan(delta.x()/(MathCoord::distance(pos1_, pos2_)+delta.y()));
	
	if (az<0)
		az = 2*M_PI+az;
	
	return az;
}

QPointF MathCoord::projectPoint(QPointF pos1_, qreal azimuth_, qreal distance_)
{
	QPointF projection;
	
	projection.setX(pos1_.x()+distance_*qSin(azimuth_));
	projection.setY(pos1_.y()+distance_*qCos(azimuth_));
	
	return projection;
}

qreal MathCoord::determinant(QPointF pos1_, QPointF pos2_, QPointF pos3_)
{
	QPointF delta1 = pos2_-pos1_;
	QPointF delta2 = pos3_-pos1_;
	
	return delta1.x()*delta2.y()-delta1.y()*delta2.x();
}

bool MathCoord::areAligned(QPointF pos1_, QPointF pos2_, QPointF pos3_)
{
	if(qFabs(MathCoord::determinant(pos1_, pos2_, pos3_))<0.001)
		return true;
		
	return false;
}

qreal MathCoord::dotProduct(QPointF pos1_, QPointF pos2_, QPointF pos3_)
{
	QPointF v1 = pos2_ - pos1_;
	QPointF v3 = pos3_ - pos1_;
	
	return v1.x() * v3.x() + v1.y() * v3.y();
}

bool MathCoord::isBetween(QPointF pos1_, QPointF pos2_, QPointF pos3_)
{
	// Check if pos1_ is between pos2_ and pos_3
	
	qreal dot1 = MathCoord::dotProduct(pos2_, pos1_, pos3_);
	qreal dot2 = MathCoord::dotProduct(pos2_, pos3_, pos3_);
	
	if(dot1 == 0 or dot1 == dot2 or (dot1>0 and dot2>dot1))
		return true;
	
	return false;
}

QPointF MathCoord::centerCircle(QPointF pos1_, QPointF pos2_, QPointF pos3_)
{
	qreal E = pos3_.y()-pos2_.y();
	qreal F = pos2_.y()-pos1_.y();
	
	// intercept divide by 0
	if(E==0 or F==0)
		return QPointF(0,0);
	
	QPointF center;
	
	qreal A = (qPow(pos3_.x(),2)-qPow(pos2_.x(),2)+qPow(pos3_.y(),2)-qPow(pos2_.y(),2))/(2*E);
	qreal B = (qPow(pos2_.x(),2)-qPow(pos1_.x(),2)+qPow(pos2_.y(),2)-qPow(pos1_.y(),2))/(2*F);
	qreal C = (pos3_.x() - pos2_.x())/E;	
	qreal D = (pos2_.x() - pos1_.x())/F;

	// intercept divide by 0
	if(C-D == 0)
		return QPointF(0,0);

	center.setX((A-B)/(C-D));
	A = -(pos2_.x()-pos1_.x())/F;
	center.setY(A*center.x()+B);
	
	return center;
}

QPointF MathCoord::rotatePoint(QPointF origin_, QPointF pos1_, qreal angle_)
{
	QPointF delta = pos1_ - origin_;

	if(angle_ <= 0)
	{
		pos1_.setX( origin_.x() + delta.x() * qCos(qFabs(angle_)) - delta.y() * qSin(qFabs(angle_)));
		pos1_.setY( origin_.y() + delta.x() * qSin(qFabs(angle_)) + delta.y() * qCos(qFabs(angle_)));
	}
	else
	{
		pos1_.setX( origin_.x() + delta.x() * qCos(qFabs(angle_)) + delta.y() * qSin(qFabs(angle_)));
		pos1_.setY( origin_.y() - delta.x() * qSin(qFabs(angle_)) + delta.y() * qCos(qFabs(angle_)));
	}
	
	return pos1_;
}

QPointF MathCoord::scalePoint(QPointF origin_, QPointF pos1_, qreal factor_)
{
	QPointF delta = pos1_ - origin_;

	pos1_.setX( origin_.x() + delta.x() * factor_);
	pos1_.setY( origin_.y() + delta.y() * factor_);
	
	return pos1_;
}
