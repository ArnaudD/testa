#include "core/support/Layer.h"

Layer::Layer(QString name_, QColor color_, bool isVisible_, int level_, bool current_)
{
	m_name = name_;
	m_color = color_;
	m_isVisible = isVisible_;
	m_level = level_;
	m_current = current_;
}

bool Layer::isCurrent() const
{
	return m_current;
}

QString Layer::getName() const
{
	return m_name;
}

QColor Layer::getColor() const
{
	return m_color;
}

bool Layer::isVisible() const
{
	return m_isVisible;
}

int Layer::getLevel() const
{
	return m_level;
}

void Layer::setCurrent(bool current_)
{
	m_current = current_;
}

void Layer::setName(QString name_)
{
	m_name = name_;
}

void Layer::setColor(QColor color_)
{
	m_color = color_;
}

void Layer::toggleVisibility()
{
	m_isVisible = !m_isVisible;
}

void Layer::setLevelUp()
{
	m_level++;
}

void Layer::setLevelDown()
{
	m_level--;
}
