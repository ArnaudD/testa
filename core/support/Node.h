#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <QtWidgets>

class ShapeItem;
class Scene;
class ArcItem;
class PathItem;

class Node : public QGraphicsItem
{

	public:	
		Node(Scene*, ShapeItem*, int, QList<Node*> *);
		void rotateNode(QPointF, qreal);
		void scaleNode(QPointF, qreal);
		int getRole() const;

	protected:
		QRectF boundingRect() const;
		void paint(QPainter*, const QStyleOptionGraphicsItem *, QWidget *);
		void mousePressEvent (QGraphicsSceneMouseEvent*);
		void mouseReleaseEvent (QGraphicsSceneMouseEvent*);
		void mouseMoveEvent (QGraphicsSceneMouseEvent*);

	private:
		void updateShape(QPointF);
		bool isMoving;
		ShapeItem *m_parent;
		Scene *m_scene;
		int m_role;
		QList<Node*> *m_listNodes;
		QPointF m_initPos;
};

#endif
