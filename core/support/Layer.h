#ifndef LAYER_H_INCLUDED
#define LAYER_H_INCLUDED

#include <QtWidgets>

using namespace std;

class Layer
{
	
	public:
	Layer(QString,	QColor,	bool, int, bool);
	QString getName() const;	
	QColor getColor() const;
	bool isVisible() const;
	int getLevel() const;
	bool isCurrent() const;
	
	void setCurrent(bool);
	void setName(QString);
	void setColor(QColor);
	void toggleVisibility();
	void setLevelUp();
	void setLevelDown();

	private:
	QString m_name;
	QColor m_color;
	bool m_isVisible;
	int m_level;
	bool m_current;
};

#endif
