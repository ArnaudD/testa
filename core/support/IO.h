#ifndef IO_H_INCLUDED
#define IO_H_INCLUDED

#include <QtWidgets>
#include "core/support/tinyxml2/tinyxml2.h"
#include "core/support/Scene.h"
#include "gui/View.h"
#include "core/shapes/ShapeItem.h"
#include "core/shapes/ImageItem.h"
#include "core/shapes/TextItem.h"

namespace IO
{
	bool saveTML(Scene*, View*, QString);
	bool readTML(Scene*, View*, QString);

	ShapeItem* readTMLFeature(Scene*, QString, tinyxml2::XMLElement*);
	QList<QPointF> readTMLNodes(tinyxml2::XMLElement*);
	
	void writeTMLFeature(QDir, QGraphicsItem*, tinyxml2::XMLDocument*, tinyxml2::XMLElement*);
	void writeTMLBlock(QDir, QList<QGraphicsItem*>&, QGraphicsItem*, tinyxml2::XMLDocument*, tinyxml2::XMLElement*);
}

#endif
