#include "core/support/Node.h"
#include "core/shapes/ShapeItem.h"
#include "core/support/Scene.h"
#include "core/shapes/ArcItem.h"
#include "core/shapes/PathItem.h"

Node::Node(Scene *scene_, ShapeItem *parent_, int role_, QList<Node*> *listNodes_) : QGraphicsItem()
{
	setFlag(QGraphicsItem::ItemIsMovable);

	setZValue(1000);
	hide();
	isMoving = false;

	m_role = role_;
	m_listNodes = listNodes_;
	m_parent = parent_;
	m_scene = scene_;
}

QRectF Node::boundingRect() const
{
	return QRectF(-5,-5,10,10);
}

void Node::paint(QPainter *painter_, const QStyleOptionGraphicsItem*, QWidget*)
{
	double scaleValue = scale();
	double scaleX = painter_->transform().m11();
	setScale(scaleValue / scaleX);

	painter_->setPen(QPen(QPen(Qt::darkGray,1,Qt::SolidLine)));
	painter_->setBrush(Qt::blue);

	if (isMoving == false)
		painter_->setBrush(Qt::blue);

	if (isMoving == true)
		painter_->setBrush(Qt::red);

	painter_->drawRect(-5,-5,10,10);
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent* event_)
{
	isMoving = true;
	m_initPos = event_->scenePos();
	update();

	QGraphicsItem::mousePressEvent(event_);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent* event_)
{
	// Update node position, usefull when snapping active
	isMoving = false;

	QPointF snapPos = m_scene->snapping(event_->scenePos());
	QPointF movement;
	
	// If snapping
	if(snapPos != event_->scenePos())
	{
		movement = snapPos - m_initPos + (event_->scenePos()-pos());
		moveBy(movement.x(), movement.y());
		updateShape(movement);
	}
	else
	{
		movement = event_->scenePos() - m_initPos;
		updateShape(movement);
	}

	m_scene->hideSnappingNode();

	QGraphicsItem::mouseReleaseEvent(event_);
}

void Node::mouseMoveEvent(QGraphicsSceneMouseEvent* event_) 
{
	if(isMoving)
	{
		// Snapping - just to display the symbol. No further action
		m_scene->snapping(pos());
		QPointF movement = event_->scenePos() - m_initPos;
		
		updateShape(movement);
	}

	m_initPos = event_->scenePos();
	QGraphicsItem::mouseMoveEvent(event_);
}

void Node::updateShape(QPointF movement_)
{
	switch( m_role )
		{
			case 0: // Point, text, image, main node rectangle
				m_parent->moveBy( movement_.x(), movement_.y() );
				break;
				
			case 1: // Line or polyline: reconstructing QPainterPath
			{
				PathItem* pathItem = dynamic_cast<PathItem*>(m_parent);
				pathItem->reconstructPathfromNodes();
				break;
			}
	
			case 2: // Center circle or arc : translate all nodes
			{
				// Move parent
				m_parent->moveBy( movement_.x(), movement_.y() );
				
				// Move nodes except the first one which is the one handled
				m_parent->translateItem(movement_, true);
	
				break;
			}
			
			case 3: // Handle Cercle or rectangle: do nothing
				break;

			case 4: // Handle arc
			{
				ArcItem* arc = dynamic_cast<ArcItem*>(m_parent);
				arc->setArc(m_listNodes->at(1)->pos(), m_listNodes->at(2)->pos(), m_listNodes->at(3)->pos());

				break; 
			}
				
			case 6: // Block
				QGraphicsItemGroup* block = dynamic_cast<QGraphicsItemGroup*>(m_parent);
				block->moveBy(movement_.x(), movement_.y() );
				
				// Move nodes and child's nodes. Ignore the first node.
				m_parent->translateItem(movement_, true);
			break;
		}
}

int Node::getRole() const
{
	return m_role;
}

void Node::rotateNode(QPointF origin_, qreal angle_)
{
	setPos(MathCoord::rotatePoint(origin_, pos(), angle_));
}

void Node::scaleNode(QPointF origin_, qreal factor_)
{
	setPos(MathCoord::scalePoint(origin_, pos(), factor_));
}
