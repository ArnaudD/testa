#ifndef MATHCOORD_H_INCLUDED
#define MATHCOORD_H_INCLUDED

#include <QtMath>
#include <QPointF>
#include <QRectF>

namespace MathCoord
{
	qreal angle(QPointF, QPointF, QPointF);
	qreal distance(QPointF, QPointF);
	qreal azimuth(QPointF, QPointF);
	QPointF projectPoint(QPointF, qreal, qreal);
	
	qreal determinant(QPointF, QPointF, QPointF);
	qreal dotProduct(QPointF, QPointF, QPointF);
	bool areAligned(QPointF, QPointF, QPointF);
	bool isBetween(QPointF, QPointF, QPointF);
	QPointF centerCircle(QPointF, QPointF, QPointF);
	
	QPointF rotatePoint(QPointF, QPointF, qreal);
	QPointF scalePoint(QPointF, QPointF, qreal);
}

#endif
