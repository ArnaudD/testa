#include "core/support/SnappingNode.h"
#include "core/support/Scene.h"

SnappingNode::SnappingNode(Scene* scene_) : QGraphicsItem()
{
	setZValue(100);
	m_scene = scene_;
	
	hide();
	
	m_snapToVertices = false;
	m_snapToNearest = false;
	m_snapToMiddle = false;
	m_snapToCenter = false;
	
	m_scene->addItem(this);
	
	m_statusDrawing = drawingNode::inactive;
}

void SnappingNode::snapToVertices(bool status_)
{
	m_snapToVertices = status_;
}

void SnappingNode::snapToNearest(bool status_)
{
	m_snapToNearest = status_;
}

void SnappingNode::snapToMiddle(bool status_)
{
	m_snapToMiddle = status_;
}

void SnappingNode::snapToCenter(bool status_)
{
	m_snapToCenter = status_;
}

QRectF SnappingNode::boundingRect() const
{
	return QRectF(-6, -6, 12, 12);
}

QPointF SnappingNode::computePosSnapping(QPointF mousePos_)
{
	// Compute the snapping size from pixel to scene coordinates
	qreal snappingDistance = 20/m_scene->views().at(0)->matrix().m11();
	
	QList<QGraphicsItem *> listItems = m_scene->items(mousePos_.x()-snappingDistance, mousePos_.y()-snappingDistance, 2*snappingDistance, 2*snappingDistance, Qt::IntersectsItemShape, Qt::AscendingOrder);

	// If there is intersected visible QGraphicsItem
	if(listItems.isEmpty() == false)
	{
		// Check each QGraphicsItem in the list
		foreach(QGraphicsItem *item, listItems)
		{
			ShapeItem *shape;
			shape = dynamic_cast<ShapeItem*>(item);

			// Some QGraphicsItem in the scene are not ShapeItem, disregard (SnappingPos, Nodes)
			if(shape)
			{
				// If the item is not in contruction and not in selection (the current one handled), return the closest node
				if(shape->getIsConstructing() == false and shape->isSelected() == false)
				{
					// Case snapToVertice. If not activated or no node nearby, continue
					if(m_snapToVertices == true)
					{
						QPointF snappingPos = shape->getClosestNodePos(mousePos_, snappingDistance);
						
						// If it didn't return mousePos, it means there is a valid snapping position
						if(snappingPos != mousePos_)
						{
							m_statusDrawing = drawingNode::vertices;
							setPos(snappingPos);
							show();
							return snappingPos;
						}
						else
							hide();
					}
					
					// Case snapToMiddle
					if(m_snapToMiddle == true)
					{
						// Get the snapping position
						QPointF snappingPos = shape->getMiddlePos(mousePos_, snappingDistance);
						
						// If it didn't return mousePos, it means there is a valid snapping position
						if(snappingPos != mousePos_)
						{
							m_statusDrawing = drawingNode::middle;
							setPos(snappingPos);
							show();
							return snappingPos;
						}
						else
							hide();
					}
					
					// Case snapToCenter
					if(m_snapToCenter == true)
					{
						// Get the snapping position
						QPointF snappingPos = shape->getCenterNodePos(mousePos_, snappingDistance);
						
						// If it didn't return mousePos, it means there is a valid snapping position
						if(snappingPos != mousePos_)
						{
							m_statusDrawing = drawingNode::center;
							setPos(snappingPos);
							show();
							return snappingPos;
						}
						else
							hide();
					}
					
					// Case snapToNearest. If not activated or no shape nearby, continue
					if(m_snapToNearest == true)
					{
						// Get the snapping position
						QPointF snappingPos = shape->getNearestPos(mousePos_, snappingDistance);
						
						// If it didn't return mousePos, it means there is a valid snapping position
						if(snappingPos != mousePos_)
						{
							m_statusDrawing = drawingNode::nearest;
							setPos(snappingPos);
							show();
							return snappingPos;
						}
						else
							hide();
					}
				}
				else
					hide();
			}
			else
				hide();
		}
	}

	return mousePos_;
}

void SnappingNode::paint(QPainter *painter_, const QStyleOptionGraphicsItem*, QWidget*)
{
	double scaleValue = scale();
	double scaleX = painter_->transform().m11();
	setScale(scaleValue / scaleX);

	painter_->setPen(QPen(Qt::magenta, 3, Qt::SolidLine));

	switch(m_statusDrawing)
	{
		case drawingNode::inactive:
			break;
			
		case drawingNode::vertices:
			painter_->drawRect(-6,-6,12,12);
			break;
			
		case drawingNode::nearest:
			painter_->drawLine(-6,-6,6,-6);
			painter_->drawLine(6,-6,-6,6);
			painter_->drawLine(-6,6,6,6);
			painter_->drawLine(6,6,-6,-6);
			break;
			
		case drawingNode::middle:
			painter_->drawLine(-6,-6,6,-6);
			painter_->drawLine(-6,-6,0,6);
			painter_->drawLine(0,6,6,-6);
			break;
		
		case drawingNode::center:
			painter_->drawEllipse(-6,-6,12,12);
			break;
	}
}

