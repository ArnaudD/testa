#include "core/support/Scene.h"
#include "gui/View.h"
#include "gui/Window.h"
#include <iostream>

Scene::Scene(QObject* parent_, Window *window_) : QGraphicsScene(parent_)
{
	// Scene parameters
	m_parentWindow = window_;
	setSceneRect(-10000,-10000,20000,20000);
	connect(this, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));

	// Initialization layer 0
	Layer *layer_ = new Layer("0", Qt::white, true, 0, true);
	m_layerList.append(layer_);

	// Status drawing
	m_drawingState = 0;
	m_activeFunction = drawingFunction::inactive;

	// Initialisation status constraints
	m_constraintOrtho_isActive = false;
	m_constraintSnapping_isActive = true;
	
	m_snappingDrawOn = false;
	m_snappingNode = new SnappingNode(this);
	m_snappingNode->snapToVertices(true);
	
	// Pen for drawing shapes
	penSelection.setCosmetic(true);
	penSelection.setStyle(Qt::DotLine);
	
	// Filename
	m_filename = "";
}

QString Scene::getFilename() const
{
	return m_filename;
}

void Scene::setFilename(QString filename_)
{
	m_filename = filename_;
}

void Scene::keyPressEvent(QKeyEvent *e)
{
	// Keys are working only if we are not in edition, to keep it simple at the moment
	if(m_drawingState == drawingFunction::inactive)
	{
		switch (e->key())
		{
			case Qt::Key_Escape:
			{
				if(selectedItems().size()==0)
					disableDrawingAll();
				else
					clearSelection();
				break;
			}
			case Qt::Key_Delete:
			{
				foreach (QGraphicsItem *item, selectedItems())
				{
					// If item exists
					if(item)
					{
						ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
						shape->removeFromScene();
					}
				}
				break;
			}
		}
	}

	QGraphicsScene::keyPressEvent(e);
}

void Scene::mouseMoveEvent (QGraphicsSceneMouseEvent* e)
{
	m_parentWindow->setCoordStatusBar(e->scenePos().x(), e->scenePos().y());

	// update positions
	if(m_activeFunction != drawingFunction::inactive and m_activeFunction != drawingFunction::drawSelection)
	{
		// If drawing start, update m_pos1
		if(m_drawingState == 0)
			m_pos1 = snapping(e->scenePos());

		// If drawing an intermediary point or an end point, update m_pos2
		else if(m_drawingState > 0)
			m_pos2 = snapping(e->scenePos());
	}
	// don't take snapping into account while drawing selection
	else if(m_activeFunction == drawingFunction::drawSelection)
	{
		// If drawing start, update m_pos1
		if(m_drawingState == 0)
			m_pos1 = e->scenePos();

		// If drawing an intermediary point or an end point, update m_pos2
		else if(m_drawingState > 0)
			m_pos2 = e->scenePos();
	}

	// If Ortho engaged, update m_pos2
	if (m_constraintOrtho_isActive==true and m_activeFunction != drawingFunction::drawRect and m_activeFunction != drawingFunction::drawSelection)
		m_pos2 = computePosOrtho(m_pos1, m_pos2);

	// Update moving line
	if(m_activeFunction == drawingFunction::drawLine and m_drawingState == 1)
		m_pathConstruction->updateLastPos(m_pos2);
	
	// Update moving polyline
	if(m_activeFunction == drawingFunction::drawPolyline and m_drawingState==1)
		m_pathConstruction->updateLastPos(m_pos2);

	// Update moving circle
	else if(m_activeFunction == drawingFunction::drawCircle and m_drawingState == 1)
		m_circleConstruction->setCircle(m_pos1, m_pos2);

	// Update moving rectangle
	else if(m_activeFunction == drawingFunction::drawRect and m_drawingState == 1)
		m_pathConstruction->updateRectangle(m_pos1, m_pos2);

	// Update moving arc
	else if(m_activeFunction == drawingFunction::drawArc and m_drawingState==1)
		m_pathConstruction->updateLastPos(m_pos2);

	else if(m_activeFunction == drawingFunction::drawArc and m_drawingState==2)
		m_arcConstruction->setArc(m_pos1,m_pos3,m_pos2);

	// Update moving image
	else if(m_activeFunction == drawingFunction::drawImage and m_drawingState == 1)
	{
		double height_ = fabs(m_pos2.x()-m_pos1.x())*(double)m_pixmap.height()/(double)m_pixmap.width();
		m_pos2.setY(m_pos1.y() + height_);
		
		m_pathConstruction->updateRectangle(m_pos1, m_pos2);
	}
	
	// Update moving line for translation or copy
	else if((m_activeFunction == drawingFunction::translate or m_activeFunction == drawingFunction::copy) and m_drawingState==1)
	{
		m_pathConstruction->updateLastPos(m_pos2);
		
		// Move the block under the mouse
		QPointF shift = m_pos2-m_initPos;
		m_blockMoving->QGraphicsItemGroup::moveBy(shift.x(), shift.y());
		m_blockMoving->translateItem(shift, false);
	}
	
	// Update moving line when rotating
	else if(m_activeFunction == drawingFunction::rotate and m_drawingState > 0)
	{
		m_pathConstruction->updateLastPos(m_pos2);
	
		// Rotate block
			if(m_drawingState == 2)
			{
				qreal angle = MathCoord::angle(m_pos2, m_pos1, m_initPos);
				m_blockMoving->rotateItem(m_pos1, angle);
			}
	}

	// Update moving line when scaling
	else if(m_activeFunction == drawingFunction::scale and m_drawingState > 0)
	{
		m_pathConstruction->updateLastPos(m_pos2);
	
		// Scale block
			if(m_drawingState == 2)
			{
				qreal scaleFactor = MathCoord::distance(m_pos1, m_pos2) / MathCoord::distance(m_pos1,  m_initPos);
				m_blockMoving->scaleItem(m_pos1, scaleFactor);
			}
	}

	// Update selection rectangle
	else if(m_activeFunction == drawingFunction::drawSelection and m_drawingState == 1)
	{
		if (m_pos2.y()<m_pos1.y())
			penSelection.setColor(Qt::blue);
		else
			penSelection.setColor(Qt::green);

		m_rectSelection->setPen(penSelection);
		m_rectSelection->setRect(QRectF(m_pos1,QSizeF(m_pos2.x()-m_pos1.x(),m_pos2.y()-m_pos1.y())));
	}
	
	if(m_activeFunction != drawingFunction::inactive and m_activeFunction != drawingFunction::drawSelection)
	{
		m_initPos = snapping(e->scenePos());
		
		// If Ortho engaged, update m_pos2
		if (m_constraintOrtho_isActive==true)
		m_initPos = m_pos2;
	}
	else
		m_initPos = e->scenePos();
		
	update();
	QGraphicsScene::mouseMoveEvent(e);
}

void Scene::mousePressEvent (QGraphicsSceneMouseEvent* e)
{
	if(e->button() == Qt::RightButton)
    {
		// Terminate polyline if right clic
		if(m_activeFunction == drawingFunction::drawPolyline and m_drawingState==1 and m_pathConstruction->getPath().elementCount()>2)
		{
			m_pathConstruction->removeLastElement();
			m_pathConstruction->clone();	
			disableDrawingAll();
		}
		
		// Terminate copy on right clic
		else if(m_activeFunction == drawingFunction::copy)			
			disableDrawingAll();	
	}
	else if(e->button() == Qt::LeftButton)
	{
		if(m_activeFunction != drawingFunction::inactive and m_activeFunction != drawingFunction::drawSelection)
		{
			m_initPos = snapping(e->scenePos());
		}
	
		// Add point on clic	
		if(m_activeFunction == drawingFunction::drawPoint)
		{
			new PointItem(this, searchCurrentLayer(), m_pos1);
			disableDrawingAll();
		}
		// Add text on clic
		else if(m_activeFunction == drawingFunction::drawText)
		{
			new TextItem(this, searchCurrentLayer(), m_pos1, "text");
			disableDrawingAll();
		}
		// Add line on clic
		else if(m_activeFunction == drawingFunction::drawLine)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving line
				case 0:
					initLineConstruction();
				break;
				// On second clic, create line on scene
				case 1:
					m_pathConstruction->clone();
					disableDrawingAll();
				break;
			}
		}
		// Add polyline on clic
		else if(m_activeFunction == drawingFunction::drawPolyline)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving polyline
				case 0:
					initLineConstruction();
				break;
				// On all other clics, add segment to polyline
				case 1:				
					// Small first move on the moving line in order to initialize the QPainterPath
					QPointF posInit_(0.005,0.005);
					posInit_+=m_pos2;
					m_pos1 = m_pos2;
					m_pathConstruction->lineTo(posInit_);
				break;
			}
		}
		// Add circle on clic
		else if(m_activeFunction == drawingFunction::drawCircle)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving circle
				case 0:
					m_circleConstruction = new CircleItem(this, searchCurrentLayer(), m_pos1, m_pos1, true);
					m_drawingState = 1;
				break;
				
				// On second clic, add circle to scene
				case 1:
					new CircleItem(this, searchCurrentLayer(), m_pos1, m_pos2, false);
					disableDrawingAll();
				break;
			}
		}
		// Add rectangle on clic
		else if(m_activeFunction == drawingFunction::drawRect)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving rectangle
				case 0:
					initRectConstruction();
				break;
				
				// On second clic, add rectangle to scene
				case 1:
					m_pathConstruction->clone();
					disableDrawingAll();
				break;
			}
		}
		// Add arc on clic
		else if(m_activeFunction == drawingFunction::drawArc)
		{
			switch(m_drawingState)
			{
				// On first clic, create a moving line
				case 0:
					initLineConstruction();
				break;
				// On second clic, remove moving line and add moving circle
				case 1:
					m_pathConstruction->removeFromScene();
					m_pos3 = m_pos2;
					m_arcConstruction = new ArcItem(this, searchCurrentLayer(), m_pos1, m_pos3, m_pos2, true);					
					m_drawingState = 2;
				break;
				// On third clic, add arc to scene
				case 2:
					new ArcItem(this, searchCurrentLayer(), m_pos1, m_pos3, m_pos2, false);
					disableDrawingAll();
				break;
			}
		}
		// Add image on clic
		else if(m_activeFunction == drawingFunction::drawImage)
		{
			switch(m_drawingState)
			{
				// On first clic, add moving rectangle
				case 0:
					initRectConstruction();
				break;
				// On second clic, remove moving rectangle and add image to scene
				case 1:
					ImageItem* imageItem = new ImageItem(this, searchCurrentLayer(), m_pos1, m_pixmap.transformed(QTransform::fromScale(1, -1),Qt::SmoothTransformation), m_filenamePixmap);
					
					qreal factor = fabs(m_pos2.x()-m_pos1.x())/(qreal)m_pixmap.width();
					imageItem->scaleItem(m_pos1, factor);
					
					disableDrawingAll();
				break;
			}
		}
		// Add line on clic to represent translation
		else if(m_activeFunction == drawingFunction::translate)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving line
				case 0:
					// Create construction line to represent the vector moving
					initLineConstruction();

					// Create block representing the objects moving
					m_blockMoving = new BlockItem(this, searchCurrentLayer(), m_pos1, m_blockSelection, true);
					m_blockMoving->QGraphicsItemGroup::setSelected(true);
				break;
				// On second clic, remove line from scene and execute the transformation on selection
				case 1:
					m_blockMoving->explode();					
					disableDrawingAll();
				break;
			}
		}
		// Rotation and scale
		else if(m_activeFunction == drawingFunction::rotate or m_activeFunction == drawingFunction::scale)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving line
				case 0:
					initLineConstruction();
				break;
				case 1:
						// If Ortho engaged, update m_initPos 
						if (m_constraintOrtho_isActive==true)
						{
							m_initPos = computePosOrtho(m_pos1, m_pos2);
						}
					
						// second clic: save the position
						m_pos3 = m_pos2;
						m_drawingState = 2;
						
						// Create block representing the objects rotating
						m_blockMoving = new BlockItem(this, searchCurrentLayer(), m_pos1, m_blockSelection, true);
						m_blockMoving->QGraphicsItemGroup::setSelected(true);
						break;
				case 2:
						m_blockMoving->explode();
						disableDrawingAll();
				break;
			}
		}
		// Add line on clic to represent copy
		else if(m_activeFunction == drawingFunction::copy)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving line
				case 0:
				{
					// Create construction line to represent the vector moving
					initLineConstruction();

					// Create block of selected objects
					BlockItem* blockCopy = new BlockItem(this, searchCurrentLayer(), m_pos1, m_blockSelection, true);
					
					// Copy this block, it will be the one moving
					m_blockMoving = dynamic_cast<BlockItem*>(blockCopy->clone());
					m_blockMoving->QGraphicsItemGroup::setSelected(true);
					
					// Delete the initial block
					blockCopy->explode();
					blockCopy->removeFromScene();
				break;
				}
				// On second clic, add a copy of the moving block in the Scene
				case 1:
				{
					// If Ortho engaged, update m_initPos 
					if (m_constraintOrtho_isActive==true)
					{
						m_initPos = computePosOrtho(m_pos1, m_pos2);
					}
					
					BlockItem* blockCopy = dynamic_cast<BlockItem*>(m_blockMoving->clone());
					blockCopy->explode();
					blockCopy->removeFromScene();
				}
				break;
			}
		}
		// On clic, add a selection rectangle
		else if(m_activeFunction == drawingFunction::drawSelection)
		{
			switch(m_drawingState)
			{
				// On first clic, create moving rectangle
				case 0:
					{
						m_pos1 = e->scenePos();
						clearSelection();
						m_rectSelection = new QGraphicsRectItem(m_pos1.x(), m_pos1.y(), 0, 0);
						addItem(m_rectSelection);
						m_drawingState = 1;
						break;
					}
				// On second clic, execute the selection
				case 1:
					QPainterPath path;
					path.addRect(m_rectSelection->rect());

					// If second point is below first point, select only the items enclosed in the rectangle
					if (e->scenePos().y()<m_pos1.y())
						setSelectionArea(path,Qt::ContainsItemBoundingRect);
					// If second point is above first point, select intersecting rectangle
					else
						setSelectionArea(path,Qt::IntersectsItemBoundingRect);

					disableDrawingAll();
				break;
			}
		}
		// Create block on click
		else if(m_activeFunction == drawingFunction::createBlock)
		{
			new BlockItem(this, searchCurrentLayer(), m_pos1, m_blockSelection, false);
			disableDrawingAll();
		}
		else
			QGraphicsScene::mousePressEvent(e);
	}
}

void Scene::selectionChanged()
{
	// If unselect, unactivate font actions
	if(selectedItems().size()==0)
	{
		m_parentWindow->setFontActions(false);
	}
	// If select
	else
	{
		foreach (QGraphicsItem *item, selectedItems())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, activate font actions
				if (textItem_)
				{
					m_parentWindow->setFontActions(true);
					return;
				}
			}
	}
}

Layer* Scene::searchLayer(QString getName_Layer)
{
	foreach(Layer* Layer_, m_layerList)
	{
		if( Layer_->getName() == getName_Layer)
			return Layer_;
	}

	return NULL;
}

Layer* Scene::searchCurrentLayer()
{
	foreach(Layer* Layer_, m_layerList)
	{
		if( Layer_->isCurrent() == true)
			return Layer_;
	}

	return NULL;
}

void Scene::toggleLayerVisibility()
{
	Layer* layer = searchCurrentLayer();
	layer->toggleVisibility();
	
	foreach(QGraphicsItem* item, items())
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);
		
		if(shape)
		{
			shape->setFlag(QGraphicsItem::ItemIsSelectable, layer->isVisible());
		}
	}
}

QPointF Scene::computePosOrtho(QPointF pos1_, QPointF posMouse_)
{
	if(fabs(posMouse_.y()-pos1_.y())<fabs(posMouse_.x()-pos1_.x()))
		posMouse_.setY(pos1_.y());
	else if(fabs(posMouse_.y()-pos1_.y())>fabs(posMouse_.x()-pos1_.x()))
		posMouse_.setX(pos1_.x());

	return posMouse_;
}

void Scene::setSceneSelection(bool status_)
{
	disableDrawingAll();
	
	if(status_==false)
	{
		m_parentWindow->setSelectionButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setSelectionButton(true);
		m_activeFunction = drawingFunction::drawSelection;
		clearSelection();
	}
}

void Scene::setScenePoint(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setPointButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setPointButton(true);
		m_activeFunction = drawingFunction::drawPoint;
	}
}

void Scene::setSceneLine(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setLineButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setLineButton(true);
		m_activeFunction = drawingFunction::drawLine;
	}
}

void Scene::setScenePolyline(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setPolylineButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setPolylineButton(true);
		m_activeFunction = drawingFunction::drawPolyline;
	}
}


void Scene::setSceneCircle(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setCircleButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setCircleButton(true);
		m_activeFunction = drawingFunction::drawCircle;
	}
}

void Scene::setSceneRectangle(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setRectButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setRectButton(true);
		m_activeFunction = drawingFunction::drawRect;
	}
}

void Scene::setSceneArc(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setArcButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setArcButton(true);
		m_activeFunction = drawingFunction::drawArc;
	}
}

void Scene::setSceneText(bool status_)
{
	clearSelection();
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setTextButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setTextButton(true);
		m_activeFunction = drawingFunction::drawText;
	}
}

void Scene::setSceneImage(bool status_)
{
	if(status_==false)
	{
		m_parentWindow->setImageButton(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		m_parentWindow->setImageButton(true);
		m_activeFunction = drawingFunction::drawImage;
		
		m_filenamePixmap = m_parentWindow->openImageDialog();

		if(m_filenamePixmap=="")
		{
			m_parentWindow->setImageButton(false);
			return;
		}
		else
			m_pixmap = QPixmap(m_filenamePixmap);
	}
}

void Scene::setSceneTranslate(bool status_)
{
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setTranslateButton(false);
		m_activeFunction = drawingFunction::inactive;
		clearSelection();
	}
	else
	{
		// Copy the selection in a temporary variable. If a selection is active during the selection of the start node, it creates trouble with the snapping
		if(selectedItems().size() > 0)
		{
			m_parentWindow->setTranslateButton(true);
			m_activeFunction = drawingFunction::translate;
			
			m_blockSelection = selectedItems();
			clearSelection();
		}
		// If nothing in the selection, cancel the process
		else
		{
			m_parentWindow->setTranslateButton(false);
			m_activeFunction = drawingFunction::inactive;
		}
	}
}

void Scene::setSceneRotate(bool status_)
{
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setRotateButton(false);
		m_activeFunction = drawingFunction::inactive;
		clearSelection();
	}
	else
		// Copy the selection in a temporary variable. If a selection is active during the selection of the start node, it creates trouble with the snapping
		if(selectedItems().size() > 0)
		{
			m_parentWindow->setRotateButton(true);
			m_activeFunction = drawingFunction::rotate;
			
			m_blockSelection = selectedItems();
			clearSelection();
		}
		// If nothing in the selection, cancel the process
		else
		{
			m_parentWindow->setRotateButton(false);
			m_activeFunction = drawingFunction::inactive;
		}
}

void Scene::setSceneScale(bool status_)
{
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setScaleButton(false);
		m_activeFunction = drawingFunction::inactive;
		clearSelection();
	}
	else
	{
		// Copy the selection in a temporary variable. If a selection is active during the selection of the start node, it creates trouble with the snapping
		if(selectedItems().size() > 0)
		{
			m_parentWindow->setScaleButton(true);
			m_activeFunction = drawingFunction::scale;
			
			m_blockSelection = selectedItems();
			clearSelection();
		}
		// If nothing in the selection, cancel the process
		else
		{
			m_parentWindow->setScaleButton(false);
			m_activeFunction = drawingFunction::inactive;
		}
	}
}


void Scene::setSceneCopy(bool status_)
{
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setCopyButton(false);
		m_activeFunction = drawingFunction::inactive;
		clearSelection();
	}
	else
		// Copy the selection in a temporary variable. If a selection is active during the selection of the start node, it creates trouble with the snapping
		if(selectedItems().size() > 0)
		{
			m_parentWindow->setCopyButton(true);
			m_activeFunction = drawingFunction::copy;
			
			m_blockSelection = selectedItems();
			clearSelection();
		}
		// If nothing in the selection, cancel the process
		else
		{
			m_parentWindow->setCopyButton(false);
			m_activeFunction = drawingFunction::inactive;
		}
}


void Scene::setSceneCreateBlock(bool status_)
{
	disableDrawingAll();

	if(status_==false)
	{
		m_parentWindow->setCreateBlock(false);
		m_activeFunction = drawingFunction::inactive;
	}
	else
	{
		// Copy the selection in a temporary variable. If selection is active during the selection of the node, it creates trouble with the snapping
		if(selectedItems().size() > 0)
		{
			m_parentWindow->setCreateBlock(true);
			m_activeFunction = drawingFunction::createBlock;
			
			m_blockSelection = selectedItems();
			clearSelection();
		}
		// If no block in the selection, cancel the process
		else
		{
			m_parentWindow->setCreateBlock(false);
			m_activeFunction = drawingFunction::inactive;
		}
	}
}

void Scene::setSceneExplodeBlock()
{
	disableDrawingAll();
	
	// If objects in selection
	if(selectedItems().size() > 0)
	{
		foreach(QGraphicsItem* item, selectedItems())
		{
			BlockItem* block = dynamic_cast<BlockItem*>(item);
			
			// If there is a block in the selection, explode it
			if(block)
			{
				block->explode();
				block->removeFromScene();
			}
		}
		clearSelection();
	}
}

void Scene::disableDrawingAll()
{
	if (m_activeFunction == drawingFunction::drawPoint)
		m_parentWindow->setPointButton(false);
	else if (m_activeFunction == drawingFunction::drawLine)
	{
		m_parentWindow->setLineButton(false);

		if(m_drawingState>0)
			m_pathConstruction->removeFromScene();

	}
	else if (m_activeFunction == drawingFunction::drawPolyline)
	{
		m_parentWindow->setPolylineButton(false);

		if(m_drawingState>0)
			m_pathConstruction->removeFromScene();
	}
	else if (m_activeFunction == drawingFunction::drawCircle)
	{
		m_parentWindow->setCircleButton(false);

		if(m_drawingState>0)
			m_circleConstruction->removeFromScene();
	}
	else if (m_activeFunction == drawingFunction::drawRect)
	{
		m_parentWindow->setRectButton(false);

		if(m_drawingState>0)
			m_pathConstruction->removeFromScene();
	}
	else if (m_activeFunction == drawingFunction::drawArc)
	{
		m_parentWindow->setArcButton(false);

		if(m_drawingState==1)
			m_pathConstruction->removeFromScene();
		else if(m_drawingState==2)
			m_arcConstruction->removeFromScene();
	}
	else if (m_activeFunction == drawingFunction::drawText)
	{
		m_parentWindow->setTextButton(false);
	}
	else if (m_activeFunction == drawingFunction::drawImage)
	{
		m_parentWindow->setImageButton(false);
		
		if(m_drawingState>0)
			m_pathConstruction->removeFromScene();
	}
	else if (m_activeFunction == drawingFunction::translate or m_activeFunction == drawingFunction::rotate or m_activeFunction == drawingFunction::scale or m_activeFunction == drawingFunction::copy)
	{
		m_parentWindow->setTranslateButton(false);
		m_parentWindow->setRotateButton(false);
		m_parentWindow->setScaleButton(false);
		m_parentWindow->setCopyButton(false);
		
		if(m_drawingState>0)
		{
			m_pathConstruction->removeFromScene();
			m_blockMoving->removeFromScene();
		}
	}
	else if(m_activeFunction == drawingFunction::drawSelection)
	{
		m_parentWindow->setSelectionButton(false);

		if(m_drawingState>0)
			delete m_rectSelection;
	}
	else if(m_activeFunction == drawingFunction::createBlock)
	{
		m_parentWindow->setCreateBlock(false);
	}

	// If TextItem in edition, unactivate all
	foreach (QGraphicsItem *item, items())
		{
			TextItem* textItem_;
			textItem_ = dynamic_cast<TextItem*>(item);

			if (textItem_)
				textItem_->outFocus();
		}

	m_activeFunction = drawingFunction::inactive;
	m_snappingDrawOn=false;
	m_snappingNode->hide();
	m_drawingState = 0;
}

void Scene::toggleOrtho()
{
	if (m_constraintOrtho_isActive == true)
	{
		m_constraintOrtho_isActive = false;
	}
	else
	{
		m_constraintOrtho_isActive = true;
	}
}

void Scene::toggleSnapping()
{
	if (m_constraintSnapping_isActive == true)
	{
		m_constraintSnapping_isActive = false;
		m_snappingDrawOn=false;
		m_snappingNode->hide();
	}
	else
	{
		m_constraintSnapping_isActive = true;
	}
}

void Scene::changeLayerSelectedItems(QString newLayerName_)
{
	for(int a=0;a<selectedItems().size();a++)
	{
		ShapeItem *item = dynamic_cast<ShapeItem*>(selectedItems()[a]);
		item->setLayer(searchLayer(newLayerName_));
	}
	update();
}

QPointF Scene::snapping(QPointF e_)
{
	QPointF pos2_ = e_;
		
		// If snapping is activated, calculate the snapping position
		if(m_constraintSnapping_isActive==true)
			pos2_ = m_snappingNode->computePosSnapping(e_);

	return pos2_;
}

QList<Layer*> *Scene::getListLayers()
{
		return &m_layerList;
}

void Scene::deleteItemsLayer(int LayerIndex_)
{
	foreach(QGraphicsItem* item, items())
	{
		ShapeItem* shape = dynamic_cast<ShapeItem*>(item);

		if(shape)
		{
			if(shape->getLayer()->getName() == m_layerList[LayerIndex_]->getName())
				shape->removeFromScene();
		}
	}

	delete m_layerList[LayerIndex_];
	m_layerList.erase(m_layerList.begin()+LayerIndex_);
}

void Scene::hideSnappingNode()
{
	m_snappingDrawOn=false;
	m_snappingNode->hide();
}

bool Scene::isOrthoEngaged()
{
	return m_constraintOrtho_isActive;
}

bool Scene::isSnappingEngaged()
{
	return m_constraintSnapping_isActive;
}

bool Scene::isDrawingActive()
{
	if(m_activeFunction > drawingFunction::inactive)
	return true;

	return false;
}

void Scene::applyFontTexts(QFont font_)
{
	foreach (QGraphicsItem *item, selectedItems())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, apply the font
				if (textItem_)
					textItem_->setFont(font_);
			}
}

void Scene::applyColorTexts(QColor color_)
{
	foreach (QGraphicsItem *item, selectedItems())
			{
				TextItem* textItem_;
				textItem_ = dynamic_cast<TextItem*>(item);

				// If there is at least 1 TextItem in the selection, apply the color
				if (textItem_)
					textItem_->setCustomColor(color_);
			}
}

void Scene::initLineConstruction()
{
	m_pathConstruction = new PathItem(this, searchCurrentLayer(), m_pos1, false, true);
						
	// Small first move on the moving line in order to initialize the QPainterPath
	QPointF posInit_(0.005,0.005);
	posInit_+=m_pos1;
	m_pathConstruction->lineTo(posInit_);
	m_drawingState = 1;
}

void Scene::initRectConstruction()
{
	m_pathConstruction = new PathItem(this, searchCurrentLayer(), m_pos1, true, true);
						
	// Small first move on the moving line in order to initialize the QPainterPath
	QPointF posInit(0.005,0.005);
	posInit+=m_pos1;
	m_pathConstruction->initRectangle(posInit);

	m_drawingState = 1;
}

enum drawingFunction::state Scene::getActiveFunction() const
{
	return m_activeFunction;
}

SnappingNode* Scene::getSnappingNode() const
{
	return m_snappingNode;
}

Scene::~Scene()
{
	foreach(Layer* layer_, m_layerList)
		delete layer_;
}


