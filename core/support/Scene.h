#ifndef SCENE_H_INCLUDED
#define SCENE_H_INCLUDED

#include <QtWidgets>

#include "core/support/Node.h"
#include "core/shapes/ShapeItem.h"
#include "core/shapes/PathItem.h"
#include "core/shapes/CircleItem.h"
#include "core/shapes/PointItem.h"
#include "core/shapes/ArcItem.h"
#include "core/shapes/TextItem.h"
#include "core/shapes/ImageItem.h"
#include "core/shapes/BlockItem.h"
#include "core/support/Layer.h"
#include "core/support/SnappingNode.h"
#include "core/support/MathCoord.h"

class Window;
class View;

namespace drawingFunction
{
	enum state{	inactive,
			drawSelection,
			drawPoint,
			drawLine,
			drawPolyline,
			drawCircle,
			drawRect,
			drawArc,
			drawText,
			drawImage,
			translate,
			rotate,
			scale,
			copy,
			createBlock };
};

class Scene : public QGraphicsScene
{
	Q_OBJECT

	public:
		Scene(QObject*, Window*);
		~Scene();
		
		QList<Layer *> *getListLayers();
		void changeLayerSelectedItems(QString);
		void deleteItemsLayer(int);

		enum drawingFunction::state getActiveFunction() const;
		bool isDrawingActive();
		void disableDrawingAll();
		
		void applyFontTexts(QFont);
		void applyColorTexts(QColor);
		
		Layer* searchLayer(QString);
		Layer* searchCurrentLayer();
		void toggleLayerVisibility();
		
		bool isOrthoEngaged();
		bool isSnappingEngaged();
		QPointF snapping(QPointF);
		void hideSnappingNode();
		SnappingNode* getSnappingNode() const;
		
		void saveFile(QString);
		QString getFilename() const;
		void setFilename(QString);
		
	public slots:
		void setSceneSelection(bool);
		void setScenePoint(bool);
		void setSceneLine(bool);
		void setScenePolyline(bool);
		void setSceneCircle(bool);
		void setSceneRectangle(bool);
		void setSceneArc(bool);
		void setSceneText(bool);
		void setSceneImage(bool);
		void setSceneTranslate(bool);
		void setSceneRotate(bool);
		void setSceneScale(bool);
		void setSceneCopy(bool);
		void setSceneCreateBlock(bool);
		void setSceneExplodeBlock();
	
		void toggleOrtho();
		void toggleSnapping();
		
	protected:
		virtual void mouseMoveEvent (QGraphicsSceneMouseEvent *e);
		virtual void mousePressEvent (QGraphicsSceneMouseEvent *e);
		virtual void keyPressEvent (QKeyEvent *e);

	private slots:
		void selectionChanged();

	private:
		void initLineConstruction();
		void initRectConstruction();
		
		bool m_modifyTranslate_isActive;
		bool m_modifyRotate_isActive;

		bool m_constraintOrtho_isActive;
		bool m_constraintSnapping_isActive;
		bool m_snappingDrawOn;
		
		QPointF computePosOrtho(QPointF, QPointF);
		
		int m_drawingState;
		drawingFunction::state m_activeFunction;
		
		QPointF m_pos1, m_pos2, m_pos3, m_posSnapping, m_initPos;
		SnappingNode* m_snappingNode;

		Window *m_parentWindow;

		QList<Layer *> m_layerList;

		CircleItem *m_circleConstruction;
		ArcItem *m_arcConstruction;
		QGraphicsRectItem *m_rectSelection;
		PathItem *m_pathConstruction;
		
		QPen penSelection;

		QPixmap m_pixmap;
		QString m_filenamePixmap;
		
		QList<QGraphicsItem*> m_blockSelection;
		BlockItem* m_blockMoving;
		
		QString m_filename;
};

#endif
