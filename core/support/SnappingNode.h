#ifndef SNAPPINGNODE_H_INCLUDED
#define SNAPPINGNODE_H_INCLUDED

#include <QtWidgets>

namespace drawingNode
{
	enum state{
			inactive,
			vertices, 
			nearest,
			middle,
			center
			};
};

class Scene;

class SnappingNode : public QGraphicsItem
{
	public:	
		SnappingNode(Scene*);
		
		QPointF computePosSnapping(QPointF);
		
		void snapToVertices(bool);
		void snapToNearest(bool);
		void snapToMiddle(bool);
		void snapToCenter(bool);

	protected:
		QRectF boundingRect() const;
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	private:
		Scene* m_scene;
		bool m_snapToVertices;
		bool m_snapToNearest;
		bool m_snapToMiddle;
		bool m_snapToCenter;
		
		drawingNode::state m_statusDrawing;
};

#endif
