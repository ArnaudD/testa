#ifndef SCENE3D_H_INCLUDED
#define SCENE3D_H_INCLUDED

#include <QtWidgets>
#include <QGLWidget>

class Scene3D : public QGLWidget
{

	public:
		explicit Scene3D(QWidget*);
	protected:
		void initializeGL();
		void resizeGL(int,int);
		void paintGL();
};

#endif
