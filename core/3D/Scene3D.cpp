#include "core/3D/Scene3D.h"

Scene3D::Scene3D(QWidget *parent_) : QGLWidget(QGLFormat(QGL::SampleBuffers), parent_)
{
}

void Scene3D::initializeGL()
{
 glClearColor(0.0, 0.0, 0.0, 0.0);
}

void Scene3D::resizeGL(int w, int h)
{
 glViewport(0, 0, (GLint)w, (GLint)h);
}

void Scene3D::paintGL()
{
}